# -*- coding: utf-8 -*-
from moodle_xml import Quiz, NumAnswer, QuestionSet 
import moodle_xml
import sys
import os
import base64
from scipy.special import binom
from math import factorial

POGLAVJE = "01 kombinatorika"
CATEGORY = "Vaje/{}".format(POGLAVJE)
QUIZ = Quiz(CATEGORY)
QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/1. skupina"

text = """ <p>V knjižnici imajo <b>{n}</b> kopij knjige iz verjetnosti in statistike,
    pri čemer so kopije oštevilčene.</p>

    <p> V ponedeljek si knjigo izposodi <b>{m}</b> študentov.  Na koliko
    načinov si lahko študentje izposodijo knjige, pri čemer je pomembno, katero
    kopijo knjige ima posamezni študent?&nbsp; {ans[0]} Na koliko načinov lahko
    kopije knjige ostanejo v knjižnici (kopije so oštevilčene)?&nbsp; {ans[1]}
    </p>
    
    <p>Čez en mesec knjige vrne v knjižnico <b>{p}</b> študentov. Knjižničarka
    knjige zloži na police, pri čemer izbira med 3 policami. Na koliko načinov
    lahko razporedi knjige na police?
    Police med seboj razlikujemo, vrstni red knjig na polici pa ni
    pomemben.&nbsp; {ans[2]} </p>

    <p>Štirinajst dni pred izpitom se teh <b>{p}</b> študentov odloči obnoviti
    svoje znanje in se med prostima urama ponovno odpravijo v knjižnico. Vsak
    izbere svojo knjigo in se
    usede za okroglo mizo z <b>{n}</b> stoli. Na koliko načinov se lahko
    usedejo za mizo, pri čemer razporeditvi štejemo za enaki, če lahko iz ene
    pridemo v drugo tako, da se vsi študenti presedejo
    za isto število stolov v isto smer?&nbsp; {ans[3]} </p>
    """

def resitev(n, m, p):
    izpos = int(1.0*factorial(n)/factorial(n-m))
    ostev = int(binom(n, m))
    police = int(3**p)
    miza = int(1.0*factorial(n-1)/factorial(n-p))
    ans = [izpos, ostev, police, miza]
    return {"ans": [NumAnswer(a) for a in ans]}


parametri = [{"n": n, "m": m, "p": p}
             for n in [9, 11]
             for m in [5, 6]
             for p in [4, 5]
            ]

QUIZ.append(QuestionSet(text, parametri, resitev, name="Knjige v knjiznici"))

text = """
    <p>Nika ima <b>{n}</b> različnih knjig, njen brat Miha pa <b>{m}</b>.</p>

    <p>Na koliko načinov lahko Nika postavi svoje knjige v vrsto na
    polico?&nbsp;{ans[0]}</p>

    <p>Na koliko načinov lahko Nika in Miha zložita vse svoje knjige na dve
    polici, če morajo Nikine knjige stati na spodnji polici, Mihove pa na
    zgornji polici?&nbsp;{ans[1]}</p>
    
    <p>Na koliko načinov lahko Nika postavi svoje knjige na dve polici? Pri tem
    polici ločimo in vrstni red knjig na polici je pomemben. &nbsp;{ans[2]}</p>

    <p>Miha svoje knjige pospravi v dva predala. Na koliko načinov lahko to
    stori? Vrstni red knjig znotraj predala ni pomemben, predala pa ločimo in
    lahko kakšen ostane prazen.&nbsp;{ans[3]}</p>

    """

def resitev2(n, m):
    ans = [factorial(n), factorial(n)*factorial(m), (n+1)*factorial(n), 2**m]
    ans = [NumAnswer(a, precision=0.5) for a in ans]
    print(n, m, ans)
    return {"ans": ans}

parametri = []
for n in [7, 8, 9]:
    for m in [4, 5, 6]:
        parametri.append({"n": n, "m": m})

QUIZ.append(QuestionSet(text, parametri, resitev2, name="Knjige na polici"))

text = """
    <p>Nika ima <b>{n}</b> različnih knjig, njen brat Miha pa <b>{m}</b>.</p>
    <p>Na koliko načinov lahko Nika postavi svoje knjige v vrsto na
    polico?&nbsp;{ans[0]}</p>

    <p>Kaj pa, če ima Nika na voljo dve polici in mora biti na vsaki polici
    vsaj ena knjiga?&nbsp;{ans[1]}</p>

    <p>Miha svoje knjige pospravi v dva predala. Na koliko načinov lahko to
    stori? Vrstni red knjig znotraj predala ni pomemben, predala pa ločimo
    in lahko kakšen ostane prazen.&nbsp;{ans[2]}</p>
    <p>Koliko pa je razporeditev, če ima Miha na voljo tri
    predale?&nbsp;{ans[3]}</p> <p>Na koliko načinov lahko Nika in Miha
    zložita vse svoje knjige na dve polici, če morajo Nikine knjige stati na
    spodnji polici, Mihove pa na zgornji polici?&nbsp;{ans[4]}</p> """
def resitev7(n, m):
    ans = (factorial(n), (n-1)*factorial(n), 2**m, 3**m,
        factorial(n)*factorial(m))
    return {"ans": [NumAnswer(a) for a in ans]}


parametri = []
for n in [7, 8, 9]:
    for m in [4, 5, 6]:
        print("n = " + str(n) + " , m = " + str(m))
        print("n! = " + str(factorial(n)))
        print("(n-1)*n! = " + str((n-1)*factorial(n)))
        print("2**m = " + str(2**m))
        print("3**m = " + str(3**m))
        print("n!*m! = " + str(factorial(n)*factorial(m)) + "\n")
        parametri.append({"n": n, "m": m})

QUIZ.append(QuestionSet(text, parametri, resitev7, name="Knjige v predalih"))


QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/2. skupina"
text = """ <p>Učenec je za domačo nalogo moral napisati vse možne nize
    iz črk <b>{crke}</b>. Učenec je napisal {dol} različnih nizov. Koliko nizov
    je pozabil napisati?&nbsp;{ans[0]}</p>

    <p>Koliko palindromov bi moral napisati? (tj. besed, ki se isto berejo
    naprej in nazaj) &nbsp;{ans[1]}</p>
    
    <p>Koliko stavkov (večina bo nesmiselnih), sestavljenih iz vsaj enega in
    največ 3 nizov, lahko tvorimo iz vseh različnih nizov, ki jih je napisal
    učenec?  Pri tem se nizi v stavku ne smejo ponavljati.&nbsp;{ans[2]}</p>

    <p>Na koliko načinov lahko vse možne nize, ki se začnejo s črko <b>{A}</b>,
    razporedimo v 3 stolpce? Pri tem stolpce med seboj ločimo, posamezen
    stolpec lahko ne vsebuje nobenega niza, vrstni red nizov znotraj stolpca pa
    ni pomemben.&nbsp;{ans[3]}</p>

 """
def resitev1(crke, m):
    dol = factorial(5)//4-m
    stavki = dol + dol*(dol-1) + dol*(dol-1)*(dol-2)
    nizi_A = factorial(4)//2
    stolpci = 3**(nizi_A)

    ans = [m, 2, stavki, stolpci]
    print(ans)
    ans = [NumAnswer(a, precision=0.1) for a in ans]

    return {"dol": round(factorial(5)/4-m), "A": crke[1], "ans":ans}

parametri = []
for crke in ['AADRR', 'OOPPT']:
    for m in [3, 4, 5]:
        parametri.append({"crke": crke, "m": m})

QUIZ.append(QuestionSet(text, parametri, resitev1, name="Besede"))


text = """<p>Maja s kupa <b>{n}</b> različnih kart izbere <b>{m}</b> kart.</p>

    <p>Na koliko različnih načinov je lahko izbrala teh <b>{m}</b> kart. Pri
    tem vrstni red izbiranja ni pomemben.&nbsp;{ans[0]}</p>

    <p>Na koliko načinov lahko Maja postavi svoje izbrane karte v
    vrsto?&nbsp;{ans[1]}</p>

    <p>Maja bo razdelila izbrane karte med dva otroka, tako da vsak dobi isto
    število kart.  Na koliko načinov lahko to stori? Vrstni red razdeljevanja
    ni pomemben.&nbsp;{ans[2]}</p>

    <p>Na koliko načinov lahko karte razdeli med tri otroke, pri čemer vsakemu
    da poljubno število kart (lahko tudi nobene)?&nbsp;{ans[3]}</p>
    """
    
def resitev3(n, m):
    ans = [int(binom(n, m)), factorial(m), int(binom(m, m/2)),3**m]
    print(ans)
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}

parametri = []
for n in [28, 32, 36]:
    for m in [6, 8]:
        parametri.append({"n": n, "m": m})

QUIZ.append(QuestionSet(text, parametri, resitev3, name="Karte"))


text = """<p>Radi bi naredili šopek iz <b>{n}</b> rož, pri čemer imamo na voljo
    rdeče, modre in rumene rože.  Rož vsake barve je vsaj {n}.</p>

    <p>Koliko različnih šopkov lahko naredimo? &nbsp;{ans[0]}</p>
    
    <p>Koliko različnih šopkov lahko naredimo, če morata biti v šopku natanko
    dve različni barvi? &nbsp;{ans[1]}</p>

    <p>Koliko različnih šopkov lahko naredimo, če morata biti v šopku največ
    dve različni barvi? &nbsp;{ans[2]}</p>

    <p>Koliko različnih šopkov lahko naredimo, če mora biti v šopku vsaj ena
    roža vsake barve? &nbsp;{ans[3]}</p> """

def resitev4(n):
    vseh = int(binom(n+3-1,n))
    dvobarvni = 3*(int(binom(n+2-1, n))-2)
    enobarvni = 3
    najvec_dvobarvni = enobarvni + dvobarvni
    trobarvni = vseh - najvec_dvobarvni
    ans = [vseh, dvobarvni, najvec_dvobarvni, trobarvni]
    print(ans)
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}

parametri = []
for n in [7, 8, 9, 10]:
    parametri.append({"n": n})

QUIZ.append(QuestionSet(text, parametri, resitev4, name="Rože"))


QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/3. skupina"

text = """<p>Koliko je permutacij števil 1,2,...,{n}, kjer se <b>1</b> pojavi
    pred <b>2</b> (ne nujno poleg)?&nbsp;{ans[0]}</p>

    <p>Koliko je permutacij števil 1,2,...,{n}, kjer <b>2</b> ne stoji takoj za
    <b>1</b>.&nbsp;{ans[1]}</p>
    
    <p>Koliko je permutacij števil 1,2,...,{n}, kjer <b>2</b> ne stoji poleg
    <b>1</b>.&nbsp;{ans[2]}</p>

    <p>Koliko je permutacij števil 1,2,...,{n}, kjer <b>1</b> stoji poleg
    <b>2</b> in <b>3</b> stoji poleg <b>4</b>.&nbsp;{ans[3]}</p> """

def resitev5(n):
    b=factorial(n)/2
    c=factorial(n)-factorial(n-1)
    d=factorial(n)-2*factorial(n-1)
    e=4*factorial(n-2)
    
    ans = [b, c, d, e]
    print(ans)
    ans = [NumAnswer(a, precision=0.5) for a in ans]
    return {"ans": ans}



parametri = []
for n in [6,7,8,9]:
    parametri.append({"n": n})

QUIZ.append(QuestionSet(text, parametri, resitev5, name="Permutacije"))

text = """<p>Iz vsake od <b>{n}</b> držav se na srečanje odpravita po 2 osebi.</p>

    <p>Sprva imajo sestanek za okroglo mizo z <b>{m}</b> sedeži, pri čemer mora
    biti na sestanku prisoten natanko en predstavnik iz vsake države. Koliko
    različnih razporeditev okrog mize se lahko pojavi, če dve razporeditvi
    štejemo za enaki natanko takrat, ko lahko pridemo iz ene v drugo z
    rotacijo?&nbsp;{ans[0]}</p>

    <p>Drugi sestanek poteka za okroglo mizo z <b>{p}</b> sedeži, na njem pa so
    prisotni vsi udeleženci srečanja.  Koliko različnih razporeditev okrog mize
    se lahko pojavi, če morata predstavnika iste države sedeti skupaj, ponovno
    pa dve razporeditvi štejemo za enaki natanko takrat, ko lahko pridemo iz
    ene v drugo z rotacijo?&nbsp;{ans[1]}</p>

    <p>Srečanje se zaključi z večerjo, na kateri se morajo predstavniki posesti
    za <b>dve</b> mizi.  Na koliko različnih načinov se lahko posedejo za
    mizama, če mizi ločimo med seboj, razporeditev
    okoli mize pa ni pomembna?&nbsp;{ans[2]} Koliko pa je takih razporeditev,
    kjer pri vsaki mizi sedita vsaj dve osebi?&nbsp;{ans[3]}</p>

    """
def resitev6(n, m, p):
    b=factorial(m)//factorial(m-n)//m*2**n
    c=factorial(n)*2**5//2//n
    d=2**(2*n)
    e=d-2-4*n
    ans = [b, c, d, e]
    print(n, m, ans)
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}



parametri = []
for n in [5, 6, 7]:
    for m in [n+1,n+2]:
        parametri.append({"n": n, "m": m, "p": 2*n})

QUIZ.append(QuestionSet(text, parametri, resitev6, name="Srečanje diplomatov"))


text = """
    <p>V posodi je <b>{n}</b> oštevilčenih kroglic, od tega je <b>{k}</b>
    rdečih, ostale so modre. Iz posode brez vračanja izberemo <b>{m}</b>
    kroglic. Vrstni red ni pomemben.</p>
    <p>Na koliko različnih načinov je lahko to naredimo?&nbsp;{ans[0]}</p>
    <p>Na koliko načinov lahko to naredimo, da bosta med izvlečenimi dve
    kroglici rdeči?&nbsp;{ans[1]}</p>
    <p>Na koliko načinov lahko izvlečene kroglice postavimo v
    vrsto?&nbsp;{ans[2]}</p>

    <p>Na koliko načinov lahko kroglice razdelimo v dve posodi? Posodi ločimo,
    lahko tudi ena ostane prazna? &nbsp;{ans[3]}</p>
    <p>Na koliko načinov lahko kroglice razdelimo v dve posode, če posod med
    seboj ne ločimo? &nbsp;{ans[4]}</p>
    """
def resitev8(n, k, m):
    ans = (int(binom(n, m)), int(binom(k,2)*binom(n-k, m-2)), factorial(m), 2**m,
        2**(m-1))
    print(n,k,m)
    print(ans)
    print("")
    return {"ans":[NumAnswer(a) for a in ans]}


parametri = []
for n in [14, 15]:
    for m in [5, 6]:
        for k in [9, 7]:
            parametri.append({"n": n, "m": m, "k": k})

QUIZ.append(QuestionSet(text, parametri, resitev8, name="Kroglice"))

with open(POGLAVJE.replace(" ","_")+".xml", "wb") as file_pointer:
    file_pointer.write(str(QUIZ).encode())
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())
os.system("latexmk -pdf {}".format("vs_kvizi.tex"))
