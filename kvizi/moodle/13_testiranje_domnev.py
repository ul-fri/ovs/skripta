#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 15. teden, 12. kviz (intervali zaupanja)/1. skupina"
FILE_NAME = "vs_kviz_12_1_1"

def naloga(n,barva,barva2,p,beta,beta2):
    text1 = """V nekem velikem mestu so med lastniki avtomobila delali anketo o barvi njihovega avtomobila. Možni odgovori so bili
    'crna', 'bela', 'rdeca', 'modra', 'zelena', 'srebrna', 'drugo'. Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Kolikšen delež anketirancev ima avto barve {barva}?&nbsp;{ans[0]} </p>
    """

    #<p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.9 za delež pojavitve barve {barva2} v mestu?
    #{{1:MC:Studentova z {n1} prostostnimi stopnjami~Studentova z {n2} prostostnimi stopnjami~hi-kvadrat z {n1} prostostnimi stopnjami~hi-kvadrat
    #    z {n2} prostostnimi stopnjami~=standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    text2="""
    <p>Koliko je spodnja meja intervala zaupanja stopnje zaupanja {beta} za delež lastnikov avtomobil, ki imajo avto barve {barva}?&nbsp;{ans[1]} </p>

    <p>Denimo, da so v nekem drugem mestu prav tako naredili anketo med 200 lastniki avtomobilov o barvi njihovega avtomobila.
       Delež barve {barva2} je bil {p}. Na podlagi tega so izračunali interval zaupanja stopnje zaupanja \\(\\beta\\)
       in dobili dolžino intervala enako {velikost}. Koliko je \\(\\beta\\)?&nbsp; {ans[2]}</p>"""

    text=text1+text2

    moznosti=['crna', 'bela', 'rdeca', 'modra', 'zelena', 'srebrna', 'drugo']
    m=len(moznosti)
    odgovori=[]
    for i in range(n):
        odgovori.append(moznosti[randint(0,m-1)])
    print(odgovori)

    k=odgovori.count(barva)
    
    delez=1.0*k/n

    sigma=sqrt(delez*(1-delez))
    z=norm.isf(1-(1+beta)/2)
    Delta=sigma*z/sqrt(n)

    spMeja=delez-Delta

    z2=norm.isf(1-(1+beta2)/2)
    velikost=round(2*sqrt(p*(1-p))/sqrt(200)*z2,5)
    
    ans=[delez,spMeja,beta2]
    print(barva,p,beta2,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,barva=barva,p=p,barva2=barva2,beta=beta,velikost=velikost,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
n=124
barva2='srebrna'
beta=0.9
for barva in ['bela','modra']:
    for p in [0.11,0.14]:
        for beta2 in [0.86,0.93]:
            text = naloga(n,barva,barva2,p,beta,beta2)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

# coding: utf-8

# In[7]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import sys
import base64
from scipy.stats import norm, t, chi2, binom,poisson
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
 
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/2. skupina"
FILE_NAME = "vs_kviz_12_2_1"
 
def naloga(st_bonbonov,alpha):
    ans=[]
    pricakovane=[0.5,0.2,0.15,0.1,0.05]
    st_rumenih=st_bonbonov-45-17-5-9
    for i in range(5):
        ans.append(st_bonbonov*pricakovane[i])
        pricakovane[i]=st_bonbonov*pricakovane[i]
 
    opazene=[45,17,st_rumenih,5,9]
    TS=0
    for i in range(5):
        TS+=(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)

    spod_meja=chi2.isf(alpha,4)
    ans.append(spod_meja)

    print(st_bonbonov,alpha,TS,ans)
    
    for i in range(5):
        ans[i] = "{{1:NUMERICAL:={}:0.01}}".format(ans[i])
    ans[5] = "{{5:NUMERICAL:={}:0.01}}".format(ans[5])
    ans[6] = "{{5:NUMERICAL:={}:0.01}}".format(ans[6])

    
    text1= """<p>
    V tovarni bonbonov trdijo, da je v vrečki bonbonov 50% rdečih, 20% modrih, 15% rumenih, 10% zelenih in 5% oranžnih bonbonov.
    Kupimo vrečko s {st_bonbonov} bonboni in v njej najdemo 45 rdečih, 17 modrih, {st_rumenih} rumenih, 5 zelenih in 9 oranžnih bonbonov.
    Pri stopnji značilnosti \\(\\alpha={alpha}\\) testiramo ničelno domnevo (\\(H_0\\)),
    da je porazdelitev barv bonbonov res taka, kot trdi tovarna.
    </p>
 
    <p>Koliko so pričakovana števila bonbonov posamezne barve v kupljeni vrečki?</p>
    rdeča:&nbsp; {ans[0]}, modra:&nbsp; {ans[1]}, rumena:&nbsp; {ans[2]}, zelena:&nbsp; {ans[3]},
    oranžna:&nbsp; {ans[4]}
 
    <p>Vrednost testne statistike je:&nbsp; {ans[5]}</p>

    <p>Spodnja meja zavrnitvenega območja ničelne hipoteze je:&nbsp; {ans[6]}</p>"""

    
    if spod_meja<TS:
        text2="""<p>Rezultat testa je:
            {{5:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>
            """

    if spod_meja>TS:
        text2="""<p>Rezultat testa je:
            {{5:MC:~\\(H_0\\) zavrnemo~\\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>
            """    

    text=text1+text2
    
    cloze = text.format(alpha=alpha,st_bonbonov=st_bonbonov,st_rumenih=st_rumenih,ans=ans)
    return cloze
     
 
 
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
 
questions = ""
count = 1
 
for st_bonbonov in [100,101]:
    for alpha in [0.05,0.01]:
        text = naloga(st_bonbonov,alpha)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1
 
 
 
# moodle XML export
 
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
 
    </category>
  </question>
 
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
 
  {questions}
 
</quiz>
"""
# read the code
 
with open(sys.argv[0]) as fp:
    CODE = fp.read()
 
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
 
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)


# coding: utf-8

# In[3]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import sys
import base64
from scipy.stats import norm, t, chi2, binom,poisson
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
 
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/3. skupina"
FILE_NAME = "vs_kviz_12_3_1"
 
def naloga(st_manjkajocih,alpha):
    ans=[]
    pricakovane=[]
    frekvenca_torek=st_manjkajocih-125-85-94-108
    for i in range(5):
        f_i=1.0/5
        if i==0: ans.append(st_manjkajocih*f_i)
        pricakovane.append(st_manjkajocih*f_i)
    print("Pričakovano število manjkajočih je {}.".format(pricakovane))
 
    opazene=[125,frekvenca_torek,85,94,108]
    print("Opaženo število manjkajočih je {}.".format(opazene))
    TS=0
    for i in range(5):
        TS+=(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)

    spod_meja=chi2.isf(alpha,4)
    ans.append(spod_meja)

    print(ans)
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]

    text1 = """<p>
    Vodilne v veliki tovarni je zanimalo, ali je število manjkajočih zaposlenih odvisno od dneva v tednu. Za testni primer so
    vzeli naključen teden, ko je skupno manjkalo {st_manjkajocih} zaposlenih, in ugotovili, da je v ponedeljek manjkalo 125, 
    v torek {frekvenca_torek}, v sredo 85, v četrtek 94 in v petek 108 ljudi.
    Pri stopnji značilnosti \\(\\alpha={alpha}\\) testiramo ničelno domnevo (\\(H_0\\)),
    da je število manjkajočih neodvisno od dneva v tednu.
    </p>
 
    <p>Koliko je pričakovano število manjkajočih na poljuben dan v testnem tednu?&nbsp; {ans[0]}</p>
 
    <p>Vrednost testne statistike je:&nbsp; {ans[1]}</p>

    <p>Spodnja meja zavrnitvenega območja ničelne hipoteze je:&nbsp; {ans[2]}</p>
    """
    if spod_meja>TS:
        text2="""<p>Rezultat testa je:
        {{1:MC:~\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>"""

    if spod_meja<TS:
        text2="""<p>Rezultat testa je:
        {{1:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>"""

    text=text1+text2
    cloze = text.format(st_manjkajocih=st_manjkajocih,frekvenca_torek=frekvenca_torek,ans=ans,alpha=alpha)
    return cloze
     
 
 
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
 
questions = ""
count = 1
 
for st_manjkajocih in [498,502]:
    for alpha in [0.01,0.03]:
        text = naloga(st_manjkajocih,alpha)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1
 
 
 
# moodle XML export
 
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
 
    </category>
  </question>
 
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
 
  {questions}
 
</quiz>
"""
# read the code
 
with open(sys.argv[0]) as fp:
    CODE = fp.read()
 
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
 
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm, t, chi2
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/4. skupina"
FILE_NAME = "vs_kviz_12_4_1"

def naloga(alpha,sigma):

    odklon=0
    while odklon<sigma:
        meritve=[]
        for i in range(100):
            meritve.append(randint(44,58))
        
        odklon=std(meritve,None,None,None,1)
        TS=(len(meritve)-1)*odklon**2/sigma**2
        spod_meja=chi2.isf(alpha,len(meritve)-1)

        ans=[TS,spod_meja]
        print(alpha,sigma,odklon,ans)
        ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
        
        text1 = """
        <p>Naredili smo raziskavo o normalno porazdeljeni slučajni spremenljivki
        \\(X\\sim N(\\mu,\\sigma)\\). Pridobljeni meritve v raziskavi
        so bile naslednje:</p>

        <pre>{meritve}</pre>

        <p>Na podlagi meritev smo naredili nekaj statističnih testov stopnje značilnosti \\(\\alpha={alpha}\\) o različnih parametrih.
        Spodnja vprašanja se nanašajo na te teste.</p>

        <p>Testiramo ničelno domnevo \\(H_0:\\sigma={sigma}\\) proti alternativi
        \\(H_1:\\sigma> {sigma}\\). Vrednost testne statistike je {ans[0]}. Spodnja meja zavrnitvenega območja je {ans[1]}.
        Rezultat testa \\(H_0\\) proti \\(H_1\\) pa je:"""

        if spod_meja<TS:
            text2="""
            {{1:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}
            </p>
            """
        if spod_meja>TS:
            text2="""
            {{1:MC:~\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}
            </p>
            """

        text=text1+text2
        cloze = text.format(alpha=alpha,sigma=sigma,meritve=meritve,ans=ans)
        return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1

for alpha in [0.05,0.07]:
    for sigma in [3.8,4.0]:
        text = naloga(alpha,sigma)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm, t, chi2
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/5. skupina"
FILE_NAME = "vs_kviz_12_5_1"

def naloga(alpha,n,povp,odklon):

    i=randint(0,1)
    
    if i%2==0:
        TS=(povp-6.0)/odklon*sqrt(n)
        meja=t.isf(alpha/2,n-1)
        p_vrednost=2*t.sf(abs(TS),n-1)
        ans=[TS,p_vrednost]
        print(i,alpha,n,povp,odklon,ans)
        ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
        
        text1 = """
        <p>V tovarni izdelujejo žeblje različnih dolžin. Iz škatle,
        kjer naj bi bili žeblji dolžine $6.0$ cm, izberemo vzorec {n}
        žebljev in izračunamo, da je povprečna dolžina žeblja ${povp}$ cm,
        vzorčni odklon pa ${odklon}$ cm. Predpostavimo, da je dolžina žeblja
        normalno porazdeljena slučajna spremenljvika.
        Pri stopnji značilnosti $\\alpha={alpha}$ testirajte domnevo tovarne o dolžini žeblja proti
        dvostranski alternativi. Vrednost testne statistike je {ans[0]}.
        Rezultat testa je"""

        if abs(meja)<TS:
            text2="""
            {{1:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}
            """
        if abs(meja)>TS:
            text2="""
            {{1:MC:~\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}
            </p>
            """

        text3="""Koliko je mejna vrednost $\\alpha$, pri kateri bi bil rezultat testa drugačen? (Če ste $H_0$ zavrnili, je mejna vrednost največji
        $\\alpha$, pri katerem $H_0$ ne bi zavrnili. Sicer pa je mejna vrednost najmanjši $\\alpha$, pri katerem bi $H_0$ zavrnili.) {ans[1]}"""


        text=text1+text2+text3
        cloze = text.format(alpha=alpha,n=n,povp=povp,odklon=odklon,ans=ans)
        return cloze

    if i%2==1:
        TS=(n-1)*odklon**2/0.15**2
        meja=chi2.isf(1-alpha,n-1)
        p_vrednost=chi2.cdf(TS,n-1)
        ans=[TS,p_vrednost]
        print(i,alpha,n,povp,odklon,ans)
        ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
        
        text1 = """
        <p>V tovarni izdelujejo žeblje različnih dolžin. Iz škatle,
        kjer naj bi bili žeblji dolžine $6.0$ cm, izberemo vzorec {n}
        žebljev in izračunamo, da je povprečna dolžina žeblja ${povp}$ cm,
        vzorčni odklon pa ${odklon}$ cm. Predpostavimo, da je dolžina žeblja
        normalno porazdeljena slučajna spremenljvika.
        Pri stopnji značnilnosti $\\alpha={alpha}$ testirajte domnevo tovarne, da
        standardni odklon dolžine žeblja ne presega $0.15$ cm. (Ker bi to
        radi potrdili, naj bo to alternativna hipoteza).         
        Vrednost testne statistike je {ans[0]}.
        Rezultat testa je"""

        if meja<TS:
            text2="""
            {{1:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}
            """
        if meja>TS:
            text2="""
            {{1:MC:~\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}
            </p>
            """

        text3="""Koliko je mejna vrednost $\\alpha$, pri kateri bi bil rezultat testa drugačen? (Če ste $H_0$ zavrnili, je mejna vrednost največji
        $\\alpha$, pri katerem $H_0$ ne bi zavrnili. Sicer pa je mejna vrednost najmanjši $\\alpha$, pri katerem bi $H_0$ zavrnili.) {ans[1]}"""


        text=text1+text2+text3
        cloze = text.format(alpha=alpha,n=n,povp=povp,odklon=odklon,ans=ans)
        return cloze
    

QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1

n=40

for alpha in [0.05,0.1]:
    for povp in [5.97,6.04]:
        for odklon in [0.12,0.13]:
            text = naloga(alpha,n,povp,odklon)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

# coding: utf-8

# In[10]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import sys
import base64
from scipy.stats import norm, t, chi2, binom,poisson
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
 
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 12. kviz (testiranje hipotez)/6. skupina"
FILE_NAME = "vs_kviz_12_6_1"
 
def naloga(st_iger):
    ans=[]
    pricakovane=[]
    tri_cifre=st_iger-15-27-48
    
    for i in range(4):
        f_i=binom.pmf(i,3,1.0/2)
        ans.append(st_iger*f_i)
        pricakovane.append(st_iger*f_i)
    print("Pričakovane frekvence so {}.".format(pricakovane))
 
    opazene=[15,27,48,tri_cifre]
    print("Opažene frekvence so {}.".format(opazene))
    
    TS=0
    for i in range(4):
        TS=TS+(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)
 
    p_vrednost=chi2.sf(TS,3)
    print(p_vrednost)
    ans.append(p_vrednost)
     
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    print(ans)
    
    text1 = """<p>
    V neki igri na srečo igralec trikrat meče kovanec, njegov dobiček pa je odvisen od števila cifer.
    Na podlagi vzorca {st_iger} iger pri stopnji značilnosti \\(\\alpha=0.10\\) testiramo ničelno hipotezo, da je 
    kovanec pošten. Na vzorcu nismo nobene cifre vrgli 15-krat, eno cifro smo vrgli 27-krat, dve cifri 48-krat,
    tri cifre pa {tri_cifre}-krat.
    </p>
 
    <p>Izračunajte pričakovane frekvence rezultatov:</p>
    <p>0 cifer:&nbsp; {ans[0]}, 1 cifra:&nbsp; {ans[1]}, 2 cifri:&nbsp; {ans[2]}, 3 cifre:&nbsp; {ans[3]}.</p>
    
    <p>Vrednost testne statistike je:&nbsp; {ans[4]}</p>
    """

    if p_vrednost<0.10:
        text2="""<p>Rezultat testa je:
            {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>"""

    if p_vrednost>0.10:
        text2="""<p>Rezultat testa je:
            {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>"""

 
    text3="""<p>Koliko je mejna vrednost $\\alpha$, pri kateri bi bil rezultat testa drugačen? (Če ste $H_0$ zavrnili, je mejna vrednost največji
        $\\alpha$, pri katerem $H_0$ ne bi zavrnili. Sicer pa je mejna vrednost najmanjši $\\alpha$, pri katerem bi $H_0$ zavrnili.) {ans[5]} </p>"""
     
    text=text1+text2+text3
    cloze = text.format(st_iger=st_iger,tri_cifre=tri_cifre,ans=ans)
    return cloze
     
 
 
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
 
questions = ""
count = 1
 
for st_iger in [100,105,110,115]:
    text = naloga(st_iger)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1
 
 
 
# moodle XML export
 
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
 
    </category>
  </question>
 
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
 
  {questions}
 
</quiz>
"""
# read the code
 
with open(sys.argv[0]) as fp:
    CODE = fp.read()
 
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
 
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import randint
from numpy import std
import random

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/1. skupina"
FILE_NAME = "ovs_kviz_13_1"

def naloga(k,alpha):
    text = """V nekem velikem mestu so delali anketo o povprečni starosti obiskovalcev nakupovalnega centra (pri tem je za majhnega otroka odgovoril starš).
    V manjšem mestu, kjer je več starejših prebivalcev, je povprečna starost obiskovalcev istoimenskega centra {malo_mesto_povprecje} let. Z anketo so želeli pri stopnji značilnosti
    \\(\\alpha={alpha}\\) potrditi, da je povprečna starost obiskovalca centra v velikem mestu nižja kot v manjšem mestu (pri čemer predpostavimo, da je starost obiskovalca
    porazdeljena normalno \\(N(\\mu,\\sigma)\\) pri neznanih \\(\\mu\\) in \\(\\sigma\\)).
    Odgovori v anketi so bili naslednji:</p>

    <pre>{odgovori}</pre>

    <p>Izberite pravilne ničelno (\\(H_0\\)) in alternativno hipotezo (\\(H_1\\)), testno statistiko (\\(TS\\)) in zavrnitveno območje (\\(K_\\alpha\\)) (pri čemer
    \\(\\overline x\\) in \\(s\\) predstavljata vzorčno povprečje in vzorčni standardni odklon.</p>

    \\(H_0\\): {{1:MC:~=\\(\\mu={malo_mesto_povprecje}\\)~\\(\\mu<{malo_mesto_povprecje}\\)~\\(\\mu>{malo_mesto_povprecje}\\)}}
    \\(H_1\\): {{1:MC:~\\(\\mu={malo_mesto_povprecje}\\)~=\\(\\mu<{malo_mesto_povprecje}\\)~\\(\\mu>{malo_mesto_povprecje}\\)}}
    \\(TS\\): {{1:MC:~\\((\\bar x-{malo_mesto_povprecje})/\\sigma\\cdot {koren_n}  \\)~\\((\\bar x-{malo_mesto_povprecje})/\\sigma\\cdot {n}\\)~=\\((\\bar x-{malo_mesto_povprecje})/s
        \\cdot {koren_n} \\)~\\((\\bar x-{malo_mesto_povprecje})/s\\cdot {n} \\)}}
    \\(K_\\alpha\\): {{1:MC:~\\((-\\infty,-1.960)\\)~=(\\(-\\infty,{kvant})\\)~(\\(-\\infty,-1.645)\\)~(\\(1.960,\\infty)\\)~(\\(1.645,\\infty)\\)~\\(({kvant},\\infty)\\)}}</p>
    
    <p>Koliko je vrednost testne statistike (\\(TS\\))?&nbsp;{ans[0]}</p>

    <p>Izberite pravilni zaključek testa:
    {{4:MC:~\\(H_0\\) ne zavrnemo, saj je pravilna.~=\\(H_0\\) ne zavrnemo, čeprav ni nujno pravilna.~\\(H_0\\) zavrnemo.~\\(H_0\\) in \\(H_1\\) obe zavrnemo.}}
    </p>

    <p>Najmanj kako velik vzorec bi morali imeti, da bi bil pri istih vzorčnem povprečju in standardnem odklonu kot v zgornjem vzorcu, zaključek testa drugačen?</p>&nbsp;{ans[1]}</p>

    <p>Najmanj koliko bi morala biti stopnja značilnosti \\(\\alpha\\), da bi bil na osnovi zgornjega vzorca zaključek testa drugačen?&nbsp;{ans[2]}</p>

    """
    
    odgovori=[]
    for i in range(15):
        odgovori.append(randint(1,10))
    for i in range(20):
        odgovori.append(randint(10,17))
    for i in range(64+k):
        odgovori.append(randint(18,70))
    for i in range(15):
        odgovori.append(randint(71,87))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)
    povp=1.0*sum(odgovori)/len(odgovori)
    odklon=std(odgovori,None,None,None,1)

    malo_mesto_povprecje=round(povp+2,3)
    TS=1.0*(povp-malo_mesto_povprecje)/odklon*sqrt(n)
    print(TS)
    
    tt=t.isf(1-alpha,n-1)
    print(tt)
    
    najmanjsa_velikost=ceil((tt*odklon/(povp-malo_mesto_povprecje))**2)
    print(najmanjsa_velikost)    

    najmanjsi_alpha=t.cdf(TS,n-1)
    print(najmanjsi_alpha)
    
    ans=[TS,najmanjsa_velikost,najmanjsi_alpha]
    ans = ["{{4:NUMERICAL:={}:0.01}}".format(TS),"{{4:NUMERICAL:={}:5}}".format(najmanjsa_velikost),"{{4:NUMERICAL:={}:0.01}}".format(najmanjsi_alpha)]
    cloze = text.format(odgovori=odgovori,malo_mesto_povprecje=round(malo_mesto_povprecje,3),alpha=alpha,kvant=round(tt,3),n=n,koren_n=round(sqrt(n),3),ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
alpha=0.05
k=0
while count<7:
    text = naloga(k,alpha)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm,t
from math import sqrt,ceil
from random import randint

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/2. skupina"
FILE_NAME = "ovs_kviz_13_2"

def naloga(n,tip_avtomobila,alpha):
    text = """V državi A so delali anketo o tipih avtomobilov, ki jih uporabljajo prebivalci starejši od 18 let. Možni odgovori so bili
    'limuzina', 'kupe', 'kabriolet', 'karavan', 'enoprostorec', 'nimam avtomobila'.
    V sosednji državi je delež uporabnikov tipa '{tip_avtomobila}' enak {delez_sosedi}. Z anketo so želeli pri stopnji značilnosti
    \\(\\alpha={alpha}\\) potrditi, da je delež uporabnikov tipa '{tip_avtomobila}' v državi A večji kot v sosednji državi.
    Odgovori v anketi so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Izberite pravilne ničelno (\\(H_0\\)) in alternativno hipotezo (\\(H_1\\)), testno statistiko (\\(TS\\)) in zavrnitveno območje (\\(K_\\alpha\\)) (pri čemer
    \\(\\overline p\\) predstavlja vzorčni delež tipa '{tip_avtomobila}').</p>

    \\(H_0\\): {{1:MC:~=delež={delez_sosedi}~delež<{delez_sosedi}~delež>{delez_sosedi}}}
    \\(H_1\\): {{1:MC:~delež={delez_sosedi}~delež<{delez_sosedi}~= delež>{delez_sosedi}}}
    \\(TS\\): {{1:MC:~\\((\\bar p-{delez_sosedi})/{sigma}\\cdot {n}  \\)~=\\((\\bar p-{delez_sosedi})/{sigma}\\cdot {koren_n}\\)~\\(({delez_sosedi}-\\bar p)/{sigma}
        \\cdot {koren_n} \\)~\\(({delez_sosedi}-\\bar p)/{sigma}\\cdot {n} \\)}}
    \\(K_\\alpha\\): {{1:MC:~\\((-\\infty,-1.960)\\)~(\\(-\\infty,-{kvant})\\)~(\\(-\\infty,-{kvant2})\\)~(\\(1.960,\\infty)\\)~=(\\({kvant},\\infty)\\)~\\(({kvant2},\\infty)\\)}}</p>


    <p>Koliko je vrednost testne statistike (\\(TS\\))?&nbsp;{ans[0]}</p>

    <p>Izberite pravilni zaključek testa:
    {{4:MC:~\\(H_0\\) ne zavrnemo, saj je pravilna.~=\\(H_0\\) ne zavrnemo, čeprav ni nujno pravilna.~\\(H_0\\) zavrnemo.~\\(H_0\\) in \\(H_1\\) obe zavrnemo.}}
    </p>

    <p>Najmanj kako velik vzorec bi morali imeti, da bi bil pri istem vzorčnem deležu zaključek testa drugačen?&nbsp;{ans[1]}</p>

    <p>Najmanj koliko bi morala biti stopnja značilnosti \\(\\alpha\\), da bi bil na osnovi zgornjega vzorca zaključek testa drugačen?&nbsp;{ans[2]}</p>
    """
    
    moznosti=['limuzina', 'kupe', 'kabriolet', 'karavan', 'enoprostorec', 'nimam avtomobila']
    odgovori=[]
    for i in range(n):
        odgovori.append(moznosti[randint(0,5)])
    print(odgovori)
        
    k=odgovori.count(tip_avtomobila)
    
    delez=1.0*k/n
    delez_sosedi=round(delez-0.01,3)
    sigma=sqrt(delez_sosedi*(1-delez_sosedi))

    TS=(delez-delez_sosedi)/sigma*sqrt(n)
    print(TS)
    
    z=norm.isf(alpha)
    tt=t.isf(alpha,n-1)
    print(z,tt)

    najmanjsa_velikost=ceil((z/(delez-delez_sosedi)*sigma)**2)
    print(najmanjsa_velikost)

    najmanjsi_alpha=1-norm.cdf(TS)
    print(najmanjsi_alpha)
    
    ans = ["{{4:NUMERICAL:={}:0.01}}".format(TS), "{{4:NUMERICAL:={}:5}}".format(najmanjsa_velikost),"{{4:NUMERICAL:={}:0.01}}".format(najmanjsi_alpha)]
    cloze = text.format(odgovori=odgovori,alpha=alpha,delez_sosedi=round(delez_sosedi,3),sigma=round(sigma,3),tip_avtomobila=tip_avtomobila,n=n,kvant=round(z,3),kvant2=round(tt,3),koren_n=round(sqrt(n),3),ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for tip_avtomobila in ['limuzina', 'kupe', 'kabriolet', 'karavan', 'enoprostorec']:
    n=100+randint(0,9)
    alpha=0.05
    text = naloga(n,tip_avtomobila,alpha)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm,t
from math import sqrt,ceil
from random import randint
import random

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/3. skupina"
FILE_NAME = "ovs_kviz_13_3"

def naloga():
    text = """V nekem velikem mestu so med lastniki avtomobilov delali anketo o letniku njihovega avtomobila.
    Pred 10 leti so ugotovili, da je letnik avtomobila normalno porazdeljena slučajna spremenljivka
    z \\(\\mu={mu}\\) in \\(\sigma={sigma}\\). V anketi privzamemo, da se \\(\\sigma\\) ni spremenil, domnevamo pa, da se
    je zaradi finančne krize \\(\mu\\) povečal za manj kot 10. To bi radi testirali pri stopnji značilnosti {alpha}.
    Odgovori v anketi so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Izberite pravilne ničelno (\\(H_0\\)) in alternativno hipotezo (\\(H_1\\)), testno statistiko (\\(TS\\)) in zavrnitveno območje (\\(K_\\alpha\\)) (pri čemer
    \\(\\overline x\\) in \\(s\\) predstavljata vzorčno povprečje in vzorčni standardni odklon.</p>

    \\(H_0\\): {{1:MC:~=\\(\\mu={mu1}\\)~\\(\\mu<{mu1}\\)~\\(\\mu>{mu1}\\)}}
    \\(H_1\\): {{1:MC:~\\(\\mu={mu1}\\)~=\\(\\mu<{mu1}\\)~\\(\\mu>{mu1}\\)}}
    \\(TS\\): {{1:MC:~=\\((\\bar x-{mu1})/{sigma}\cdot {koren_n}  \\)~\\((\\bar x-{mu1})/{sigma}\\cdot {n}\\)~\\((\\bar x-{mu1})/s
        \\cdot {koren_n} \\)~\\((\\bar x-{mu1})/s\\cdot {n} \\)}}
    \\(K_\\alpha\\): {{1:MC:~\\((-\\infty,-1.960)\\)~=(\\(-\\infty,-{kvant})\\)~(\\(-\\infty,-{kvant2})\\)~(\\(1.960,\\infty)\\)~(\\({kvant},\\infty)\\)~\\(({kvant2},\\infty)\\)}}</p

    <p>Koliko je vrednost testne statistike (\\(TS\\))?&nbsp;{ans[0]}</p>

    <p>Izberite pravilni zaključek testa:
    {{4:MC:~\\(H_0\\) ne zavrnemo, saj je pravilna.~=\\(H_0\\) ne zavrnemo, čeprav ni nujno pravilna.~\\(H_0\\) zavrnemo.~\\(H_0\\) in \\(H_1\\) obe zavrnemo.}}
    </p>

    <p>Najmanj kako velik vzorec bi morali imeti, da bi bil pri istem vzorčnem povprečju zaključek testa drugačen?</p>&nbsp;{ans[1]}</p>

    <p>Najmanj koliko bi morala biti stopnja značilnosti \\(\\alpha\\), da bi bil na osnovi zgornjega vzorca zaključek testa drugačen?&nbsp;{ans[2]}</p>
    
    """
    
    odgovori=[]
    for i in range(20):
        odgovori.append(randint(2016,2018))
    for i in range(45):
        odgovori.append(randint(2011,2015))
    for i in range(30):
        odgovori.append(randint(2003,2010))
    for i in range(15):
        odgovori.append(randint(1988,2002))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)
    
    mu=2000.3
    sigma=6.1
    alpha=0.05

    z=norm.isf(1-alpha)
    tt=t.isf(1-alpha,n-1)
    print(n,z,tt)

    kvant=-z
    kvant2=-tt
    povp=1.0*sum(odgovori)/len(odgovori)
    print(povp)
    
    TS=1.0*(povp-mu-10)/sigma*sqrt(n)
    print(TS)
        
    najmanjsa_velikost=ceil((z*sigma/(povp-mu-10))**2)
    print(najmanjsa_velikost)    

    najmanjsi_alpha=norm.cdf(TS)
    print(najmanjsi_alpha)
    

    ans = ["{{4:NUMERICAL:={}:0.01}}".format(TS),"{{4:NUMERICAL:={}:5}}".format(najmanjsa_velikost),"{{4:NUMERICAL:={}:0.01}}".format(najmanjsi_alpha)]
    cloze = text.format(odgovori=odgovori,mu=mu,mu1=mu+10,alpha=alpha,sigma=sigma,kvant=round(kvant,3),kvant2=round(kvant2,3),n=n,koren_n=round(sqrt(n),3),ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1

while count<7:
    text = naloga()
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm, t
from math import sqrt, ceil
from random import randint
import random

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/4. skupina"
FILE_NAME = "ovs_kviz_13_4"

def naloga():
    text = """
    <p>Zbrali smo podatke o dosežku vzorca učencev na nacionalnem testu iz angleščine.
    Za pozitiven rezultat je potrebno doseči vsaj 50 procentov točk.
    Na testu je bilo prejšnje leto uspešnih 93 procentov vseh učencev.
    Pri stopnji tveganja \\(\\alpha={alpha}\\) bi radi preverili domnevo, da letos delež
    uspešnih učencev ni isti kot lansko leto.
    V naslednjem seznamu so zapisane frekvence posameznih rezultatov, pri čemer
    na \\(i\\)-tem mestu stoji frekvenca učencev, ki so dosegli \\(i-1\\) procentov:</p>

    <pre>{frekvence}</pre>

    <p>Izberite pravilne ničelno (\\(H_0\\)) in alternativno hipotezo (\\(H_1\\)), testno statistiko (\\(TS\\)) in zavrnitveno območje (\\(K_\\alpha\\)) (pri čemer
    \\(\\overline p\\) predstavlja vzorčni delež uspešnih učencev.</p>

    \\(H_0\\): {{1:MC:~=\\(delež={delez_lani}\\)~\\(delež<{delez_lani}\\)~\\(delež>{delez_lani}\\)}}
    \\(H_1\\): {{1:MC:~\\(delež<{delez_lani}\\)~=\\(delež\\neq {delez_lani}\\)~\\(delež>{delez_lani}\\)}}
    \\(TS\\): {{1:MC:~\\((\\bar p-{delez_lani})/{sigma}\\cdot {n}  \\)~=\\((\\bar p-{delez_lani})/{sigma}\\cdot {koren_n}\\)~\\(({delez_lani}-
    \\bar p)/{sigma}\\cdot {koren_n} \\)~\\(({delez_lani}-\\bar p)/{sigma}\\cdot {n} \\)}}
    \\(K_\\alpha\\): {{1:MC:~\\((-\\infty,-{kvant1}) \\cup ({kvant1},\\infty) \\)~=\\((-\\infty,-{kvant2}) \\cup ({kvant2},\\infty) \\)~(\\({kvant1},
    \\infty)\\)~(\\({kvant2},\\infty)\\)~\\((-\\infty,-{kvant1})~\\((-\\infty,-{kvant2})\\)}}
    

    <p>Koliko je vrednost testne statistike (\\(TS\\))?&nbsp;{ans[0]}</p>

    <p>Izberite pravilni zaključek testa:
    {{4:MC:~\\(H_0\\) ne zavrnemo, saj je pravilna.~\\(H_0\\) ne zavrnemo, čeprav ni nujno pravilna.~=\\(H_0\\) zavrnemo.~\\(H_0\\) in \\(H_1\\) obe zavrnemo.}}
    </p>

    <p>Največ kako velik vzorec bi morali imeti, da bi bil pri istem vzorčnem deležu zaključek testa drugačen?</p>&nbsp;{ans[1]}</p>

    <p>Največ koliko bi lahko bila stopnja značilnosti \\(\\alpha\\), da bi bil na osnovi zgornjega vzorca zaključek testa drugačen?&nbsp;{ans[2]}</p>

    """
    alpha=0.05
    delez_lani=0.93

    TS=0
    kvant2=1
    
    while abs(TS)<kvant2:
        frekvence=[]
        for i in range(40):
            frekvence.append(randint(0,1))
        for i in range(20):
            frekvence.append(0)
        random.shuffle(frekvence)
        for i in range(20):
            frekvence.append(randint(4,5))
        for i in range(10):
            frekvence.append(randint(3,4))
        for i in range(11):
            frekvence.append(randint(2,3))
        
        n=sum(frekvence)
        uspesni=sum(frekvence[50:])
        delez=1.0*uspesni/n
        
        while 0.88<delez<0.93:
            frekvence[randint(0,49)]+=1
            uspesni=1
            n+=1
            delez=1.0*uspesni/n
        while 0.93<delez<0.98:
            frekvence[randint(50,100)]+=1
            uspesni+=1
            n+=1
            delez=1.0*uspesni/n
        print(frekvence)

        n=sum(frekvence)
        uspesni=sum(frekvence[50:])
        delez=1.0*uspesni/n
        print(n,uspesni,delez)

        sigma=sqrt(delez_lani*(1-delez_lani))

        kvant1=norm.isf(alpha)
        kvant2=norm.isf(alpha/2)
        print(kvant2)

        TS=(delez-delez_lani)/sigma*sqrt(n)
        print(TS)

    print(n,delez,TS,"KONEC ZANKE")

    najmanjsa_velikost=ceil((kvant2/(delez-delez_lani)*sigma)**2)
    print(najmanjsa_velikost)

    najmanjsi_alpha=2*min(norm.cdf(TS),norm.sf(TS))
    print(najmanjsi_alpha)
    

    ans = ["{{4:NUMERICAL:={}:0.01}}".format(TS), "{{4:NUMERICAL:={}:5}}".format(najmanjsa_velikost),"{{4:NUMERICAL:={}:0.01}}".format(najmanjsi_alpha)]
    cloze = text.format(frekvence=frekvence,delez_lani=delez_lani,sigma=round(sigma,3),alpha=alpha,n=n,koren_n=round(sqrt(n),3),
                        kvant1=round(kvant1,3),kvant2=round(kvant2,3),ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1

while count<7:
    text = naloga()
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm, t
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/5. skupina"
FILE_NAME = "ovs_kviz_13_5"

def naloga():
    text = """
    <p>Naredili smo raziskavo o normalno porazdeljeni slučajni spremenljivki
    \\(X\\sim N(\\mu,\\sigma)\\). Pridobljeni meritve v raziskavi
    so bile naslednje:</p>

    <pre>{meritve}</pre>

    <p>Na podlagi meritev smo naredili nekaj statističnih testov stopnje značilnosti \\(\\alpha=0.05\\) o različnih parametrih.
    Spodnja vprašanja se nanašajo na te teste.</p>

    <p>Denimo, da smo dobili podatek, da je \\(\\sigma=5\\). Testiramo ničelno domnevo \\(H_0:\\mu=\\mu_0\\) proti alternativam
    \\(H_1:\\mu\\neq\\mu_0\\), \\(H_2:\\mu<\\mu_0\\) in \\(H_3:\\mu>\\mu_0\\). Vrednost testne statistike je {TS1}. Potem je:
    \\(\\mu_0=\\) {ans[0]}, rezultati testov pa so:</p>
    <p>\\(H_0\\) proti \\(H_1\\): {{1:MC:~\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}},</p>
    <p>\\(H_0\\) proti \\(H_2\\): {{1:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}},</p>
    <p>\\(H_0\\) proti \\(H_3\\): {{1:MC:~\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}.</p>


    <p>V testu o \\(\\sigma\\) testiramo ničelno domnevo \\(H_0:\\sigma=\\sigma_0\\) proti alternativi
    \\(H_1:\\sigma\\neq\\sigma_0\\). Vrednost testne statistike je {TS3}. Potem je:
    \\(\\sigma_0=\\) {ans[1]}, rezultat testa \\(H_0\\) proti \\(H_1\\) pa je:
    {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}
    </p>
    """

    meritve=[]
    for i in range(106):
        meritve.append(randint(44,58))
    print(meritve)
    print(mean(meritve),std(meritve,None,None,None,1))


    alpha=0.05
    TS1=-1.7
    mu0_znan_sigma=mean(meritve)-TS1*5/sqrt(len(meritve))
    print(TS1,mu0_znan_sigma)
    
    TS3=52.4
    sigma0=sqrt((len(meritve)-1)*std(meritve)**2/TS3)
    print(TS3,sigma0)

    ans = ["{{1:NUMERICAL:={}:0.01}}".format(mu0_znan_sigma), "{{2:NUMERICAL:={}:0.01}}".format(sigma0)]
    cloze = text.format(meritve=meritve,TS1=TS1,TS3=TS3,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1

while count<7:
    text = naloga()
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm, t
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/6. skupina"
FILE_NAME = "ovs_kviz_13_6"

def naloga():
    text = """
    <p>Naredili smo raziskavo o normalno porazdeljeni slučajni spremenljivki
    \\(X\\sim N(\\mu,\\sigma)\\). Pridobljeni meritve v raziskavi
    so bile naslednje:</p>

    <pre>{meritve}</pre>

    <p>Na podlagi meritev smo naredili nekaj statističnih testov stopnje značilnosti \\(\\alpha=0.05\\) o različnih parametrih.
    Spodnja vprašanja se nanašajo na te teste.</p>

    <p>Denimo, da podatka o \\(\\sigma\\) nismo imeli. Testiramo ničelno domnevo \\(H_0:\\mu=\\mu_0\\) proti alternativam
    \\(H_1:\\mu\\neq\\mu_0\\), \\(H_2:\\mu<\\mu_0\\) in \\(H_3:\\mu>\\mu_0\\). Vrednost testne statistike je {TS2}. Potem je:
    \\(\\mu_0=\\) {ans[0]}, rezultati testov pa so:</p>
    <p>\\(H_0\\) proti \\(H_1\\): {{1:MC:~\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}},</p>
    <p>\\(H_0\\) proti \\(H_2\\): {{1:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}},</p>
    <p>\\(H_0\\) proti \\(H_3\\): {{1:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}.</p>

    <p>V testu o deležu rezultatov nad 56.5 testiramo ničelno domnevo \\(H_0:delež=0.08\\) proti alternativi
    \\(H_1:delež\\neq 0.08\\). Vrednost testne statistike je {ans[1]}, rezultat testa \\(H_0\\) proti \\(H_1\\) pa je:
    {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}
    </p>
    """

    meritve=[]
    for i in range(90):
        meritve.append(randint(44,56))
    for i in range(15+randint(0,2)):
        meritve.append(randint(57,60))
    print(meritve)
    print(mean(meritve),std(meritve,None,None,None,1))
    k=0
    for podatek in meritve:
        if podatek>56.5: k+=1
    print(k,1.0*k/len(meritve))

    TS2=1.9
    mu0_neznan_sigma=mean(meritve)-TS2*std(meritve)/sqrt(len(meritve))
    print(TS2,mu0_neznan_sigma)
    
    p0=0.08
    TS3=1.0*sqrt(len(meritve))/sqrt(p0*(1-p0))*(1.0*k/len(meritve)-p0)
    print(TS3)

    ans = ["{{1:NUMERICAL:={}:0.01}}".format(mu0_neznan_sigma),"{{2:NUMERICAL:={}:0.01}}".format(TS3)]
    cloze = text.format(meritve=meritve,TS2=TS2,TS3=TS3,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1

while count<7:
    text = naloga()
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm, t, chi2
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random

NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/7. skupina"
FILE_NAME = "ovs_kviz_13_7"

def naloga():
    text = """
    <p>Naredili smo raziskavo o parametrih normalno porazdeljeno slučajno spremenljivko \\(X\sim N(\\mu,\\sigma)\\) in dobili naslednje
    rezultate:</p>

    <pre>{rezultati}</pre>

    <p>Testirajte ničelno hipotezo \\(H_0: \\mu={mu0}\\) proti alternativni hipotezi \\(H_1: \\mu<{mu0}\\) pri stopnji značilnosti \\(\\alpha=0.05\\), pri čemer je \\(TS\\) vrednost
    pripadajoče testne statistike:</p>

    <p>\\(TS=\\) {ans[0]}, sklep testa: {{1:MC:~\\(H_0\\) sprejmemo.~\\(H_0\\) zavrnemo.~=\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.~\\(H_0\\) ne zavrnemo, saj drži.}}</p>
    
    <p>Najmanj koliko bi moral biti \\(\\alpha\\), da bi bil sklep testa drugačen?&nbsp;{ans[2]}</p>

    <p>Testirajte še ničelno hipotezo \\(H_0: \\sigma={sigma0}\\) proti alternativni hipotezi \\(H_1: \\sigma>{sigma0}\\) pri stopnji značilnosti \\(\\alpha=0.10\\), pri čemer je \\(TS\\) vrednost
    pripadajoče testne statistike:</p>
    
    <p>\\(TS=\\) {ans[1]}, sklep testa: {{1:MC:~\\(H_0\\) sprejmemo.~=\\(H_0\\) zavrnemo.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.~\\(H_0\\) ne zavrnemo, saj drži.}}</p>

    <p>Koliko je najmanjši \\(\\alpha\\), da sklep testa še ostane isti?&nbsp;{ans[3]}</p>
    
    """

    rezultati=[]
    for i in range(80+randint(0,16)):
        rezultati.append(randint(150,197))
    print(rezultati)
    print(mean(rezultati),std(rezultati,None,None,None,1))

    n=len(rezultati)
    povpVz=mean(rezultati)
    odklon=std(rezultati,None,None,None,1)

    #vprasanje 1
    
    mu0=round(povpVz+2,3)
    alpha=0.05

    TS1=1.0*(povpVz-mu0)/odklon*sqrt(n)
    print("Testna statistika je {}.".format(TS1))

    c=t.isf(alpha,n-1)
    print("Intervala zavračanja je TS1<-{0}.".format(c))

    #vprasanje 2
    
    sigma0=round(odklon-2,3)
    alpha=0.10

    TS2=1.0*(n-1)*odklon**2/sigma0**2
    print("Testna statistika je {}.".format(TS2))

    c=chi2.isf(alpha,n-1)
    print("Intervala zavračanja je {0}<TS.".format(c))

    #vprasanje 3
    najmanjsi_alpha1=t.cdf(TS1,n-1)
    print(najmanjsi_alpha1)
    
    #vprasanje 4
    najmanjsi_alpha2=chi2.sf(TS2,n-1)
    print(najmanjsi_alpha2)
    
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(TS1), "{{1:NUMERICAL:={}:0.01}}".format(TS2),"{{2:NUMERICAL:={}:0.01}}".format(najmanjsi_alpha1),"{{2:NUMERICAL:={}:0.01}}".format(najmanjsi_alpha2)]
    cloze = text.format(rezultati=rezultati,mu0=round(mu0,3),sigma0=round(sigma0,3),ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1

while count<7:
    text = naloga()
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

# coding: utf-8

# In[3]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import sys
import base64
from scipy.stats import norm, t, chi2, binom,poisson
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
 
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/1. skupina"
FILE_NAME = "ovs_kviz_14_1"
 
def naloga(st_manjkajocih):
    text = """<p>
    Vodilne v veliki tovarni je zanimalo, ali je število manjkajočih zaposlenih odvisno od dneva v tednu. Za testni primer so
    vzeli naključen teden, ko je skupno manjkalo {st_manjkajocih} zaposlenih, in ugotovili, da je v ponedeljek manjkalo 125, 
    v torek {frekvenca_torek}, v sredo 85, v četrtek 94 in v petek 108 ljudi.
    Pri stopnji značilnosti \\(\\alpha=0.05\\) testiramo ničelno domnevo (\\(H_0\\)),
    da je število manjkajočih neodvisno od dneva v tednu.
    </p>
 
    <p>Koliko je pričakovano število manjkajočih na poljuben dan v testnem tednu?&nbsp; {ans[0]}</p>
 
    <p>Vrednost testne statistike je:&nbsp; {ans[1]}</p>
 
    <p>\\(p\\)-vrednost za testno statistiko je:&nbsp; {ans[2]}</p>
 
    <p>Rezultat testa je:
    {{1:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>
    """
     
    ans=[]
    pricakovane=[]
    frekvenca_torek=st_manjkajocih-125-85-94-108
    for i in range(5):
        f_i=1.0/5
        if i==0: ans.append(st_manjkajocih*f_i)
        pricakovane.append(st_manjkajocih*f_i)
    print("Pričakovano število manjkajočih je {}.".format(pricakovane))
 
    opazene=[125,frekvenca_torek,85,94,108]
    print("Opaženo število manjkajočih je {}.".format(opazene))
    TS=0
    for i in range(5):
        TS+=(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)
 
    p_vrednost=chi2.sf(TS,4)
    ans.append(p_vrednost)
    print("Vrednost testne statistike je {0}, p-vrednost pa je {1}".format(TS,p_vrednost))
     
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    ans[2]="{{1:NUMERICAL:={}:0.001}}".format(p_vrednost)
    print(ans)
    cloze = text.format(st_manjkajocih=st_manjkajocih,frekvenca_torek=frekvenca_torek,ans=ans)
    return cloze
     
 
 
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
 
questions = ""
count = 1
 
for st_manjkajocih in range(497,503):
    text = naloga(st_manjkajocih)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1
 
 
 
# moodle XML export
 
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
 
    </category>
  </question>
 
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
 
  {questions}
 
</quiz>
"""
# read the code
 
with open(sys.argv[0]) as fp:
    CODE = fp.read()
 
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
 
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)


# coding: utf-8

# In[1]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import sys
import base64
from scipy.stats import norm, t, chi2, binom
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
 
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/2. skupina"
FILE_NAME = "ovs_kviz_14_2"
 
def naloga(st_meritev):
    text = """<p>
    Naredili smo {st_meritev} meritev slučajne spremenljivke \\(X\\) in vrednosti \\(X=0\\), \\(X=1\\), \\(X=2\\), \\(X=3\\)
    v tem vrstnem redu dobili 4, 21, {frekvenca_dvojk} in 13. Pri stopnji značilnosti \\(\\alpha=0.05\\) testiramo ničelno domnevo (\\(H_0\\)),
    da je \\(X\\) porazdeljena binomsko za \\(n=6\\) in \\(p=0.25\\).
    </p>
 
    <p>Izračunajte pričakovane frekvence rezultatov:</p>
    <p>\\(X=0\\):&nbsp; {ans[0]}, \\(X=1\\):&nbsp; {ans[1]}, \\(X=2\\):&nbsp; {ans[2]}, \\(X=3\\):&nbsp; {ans[3]}.</p>
 
    <p>Vrednost testne statistike je:&nbsp; {ans[4]}</p>
 
    <p>\\(p\\)-vrednost za testno statistiko je:&nbsp; {ans[5]}</p>
 
    <p>Rezultat testa je:
    {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>
    """
     
    ans=[]
    pricakovane=[]
    frekvenca_dvojk=st_meritev-4-21-13
    for i in range(4):
        f_i=binom.pmf(i,6,0.25)
        ans.append(st_meritev*f_i)
        pricakovane.append(st_meritev*f_i)
    print("Pričakovane frekvence so {}.".format(pricakovane))
 
    opazene=[4,21,frekvenca_dvojk,13]
    print("Opažene frekvence so {}.".format(opazene))
    
    TS=0
    for i in range(4):
        TS+=(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)
 
    p_vrednost=chi2.sf(TS,3)
    ans.append(p_vrednost)
    print("Vrednost testne statistike je {0}, p-vrednost pa je {1}".format(TS,p_vrednost))
 
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    ans[5]="{{1:NUMERICAL:={}:0.001}}".format(p_vrednost)
    print(ans)
    cloze = text.format(st_meritev=st_meritev,frekvenca_dvojk=frekvenca_dvojk,ans=ans)
    return cloze
     
 
 
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
 
questions = ""
count = 1
 
for st_meritev in range(45,51):
    text = naloga(st_meritev)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1
 
 
 
# moodle XML export
 
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
 
    </category>
  </question>
 
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
 
  {questions}
 
</quiz>
"""
# read the code
 
with open(sys.argv[0]) as fp:
    CODE = fp.read()
 
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
 
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)


# coding: utf-8

# In[1]:


# coding: utf-8
 
# In[1]:
 
 
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
  
import sys
import base64
from scipy.stats import norm, t, chi2, binom,poisson
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
  
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/3. skupina"
FILE_NAME = "ovs_kviz_14_3"
  
def naloga(st_meritev):
    text = """<p>
    Naredili smo {st_meritev} meritev slučajne spremenljivke \\(X\\) in vrednosti \\(X=0\\), \\(X=1\\), \\(X=2\\), \\(X=3\\)
    v tem vrstnem redu dobili 24, 30, {frekvenca_dvojk} in 11-krat. Pri stopnji značilnosti \\(\\alpha=0.05\\) testiramo ničelno domnevo (\\(H_0\\)),
    da je \\(X\\) porazdeljena Poissonovo z \\(\\lambda=1.2\\).
    </p>
  
    <p>Izračunajte pričakovane frekvence rezultatov:</p>
    <p>\\(X=0\\):&nbsp; {ans[0]}, \\(X=1\\):&nbsp; {ans[1]}, \\(X=2\\):&nbsp; {ans[2]}, \\(X=3\\):&nbsp; {ans[3]}.</p>
  
    <p>Vrednost testne statistike je:&nbsp; {ans[4]}</p>
  
    <p>\\(p\\)-vrednost za testno statistiko je:&nbsp; {ans[5]}</p>
  
    <p>Rezultat testa je:
    {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>
    """
      
    ans=[]
    pricakovane=[]
    frekvenca_dvojk=st_meritev-24-30-11
    for i in range(4):
        f_i=poisson.pmf(i,1.2)
        ans.append(st_meritev*f_i)
        pricakovane.append(st_meritev*f_i)
    print("Pričakovane frekvence so {}.".format(pricakovane))
  
    opazene=[24,30,frekvenca_dvojk,11]
    print("Opažene frekvence so {}.".format(opazene))
     
    TS=0
    for i in range(4):
        TS+=(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)
  
    p_vrednost=chi2.sf(TS,3)
    ans.append(p_vrednost)
    print("Testna statistika je {0}, p-vrednost pa {1}.".format(TS,p_vrednost))
      
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    ans[5]="{{1:NUMERICAL:={}:0.001}}".format(p_vrednost)
    print(ans)
    cloze = text.format(st_meritev=st_meritev,frekvenca_dvojk=frekvenca_dvojk,ans=ans)
    return cloze
      
  
  
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
  
questions = ""
count = 1
  
for st_meritev in range(100,105):
    text = naloga(st_meritev)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1
  
  
  
# moodle XML export
  
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
  
    </category>
  </question>
  
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
  
  {questions}
  
</quiz>
"""
# read the code
  
with open(sys.argv[0]) as fp:
    CODE = fp.read()
  
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
  
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)


# coding: utf-8

# In[7]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import sys
import base64
from scipy.stats import norm, t, chi2, binom,poisson
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
 
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/4. skupina"
FILE_NAME = "ovs_kviz_14_4"
 
def naloga(st_bonbonov):
    text = """<p>
    V tovarni bonbonov trdijo, da je v vrečki bonbonov 50% rdečih, 20% modrih, 15% rumenih, 10% zelenih in 5% oranžnih bonbonov.
    Kupimo vrečko s {st_bonbonov} bonboni in v njej najdemo 45 rdečih, 17 modrih, {st_rumenih} rumenih, 5 zelenih in 9 oranžnih bonbonov.
    Pri stopnji značilnosti \\(\\alpha=0.05\\) testiramo ničelno domnevo (\\(H_0\\)),
    da je porazdelitev barv bonbonov res taka, kot trdi tovarna.
    </p>
 
    <p>Koliko so pričakovana števila bonbonov posamezne barve v kupljeni vrečki?</p>
    rdeča:&nbsp; {ans[0]}, modra:&nbsp; {ans[1]}, rumena:&nbsp; {ans[2]}, zelena:&nbsp; {ans[3]},
    oranžna:&nbsp; {ans[4]}
 
    <p>Vrednost testne statistike je:&nbsp; {ans[5]}</p>
 
    <p>\\(p\\)-vrednost za testno statistiko je:&nbsp; {ans[6]}</p>
 
    <p>Rezultat testa je:
    {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>
    """
     
    ans=[]
    pricakovane=[0.5,0.2,0.15,0.1,0.05]
    st_rumenih=st_bonbonov-45-17-5-9
    for i in range(5):
        ans.append(st_bonbonov*pricakovane[i])
        pricakovane[i]=st_bonbonov*pricakovane[i]
    print(pricakovane)
 
    opazene=[45,17,st_rumenih,5,9]
    TS=0
    for i in range(5):
        TS+=(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)
    print(TS)
 
    p_vrednost=chi2.sf(TS,4)
    ans.append(p_vrednost)
    print(p_vrednost)
     
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    ans[6]="{{1:NUMERICAL:={}:0.001}}".format(p_vrednost)
    print(ans)
    cloze = text.format(st_bonbonov=st_bonbonov,st_rumenih=st_rumenih,ans=ans)
    return cloze
     
 
 
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
 
questions = ""
count = 1
 
for st_bonbonov in range(100,106):
    text = naloga(st_bonbonov)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1
 
 
 
# moodle XML export
 
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
 
    </category>
  </question>
 
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
 
  {questions}
 
</quiz>
"""
# read the code
 
with open(sys.argv[0]) as fp:
    CODE = fp.read()
 
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
 
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)


# coding: utf-8

# In[37]:


# -*- coding: utf-8 -*-
 
import sys
import base64
from scipy.stats import norm, t, chi2, binom,poisson
from scipy import stats
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
 
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/5. skupina"
FILE_NAME = "ovs_kviz_14_5"
 
def naloga(st_klicev):
    text = """<p>
    Pridobili smo podatke o dolžini klicev {st_klicev} uporabnikov mobilnih telefonov.
    Klicev, krajših od 5 minut je bilo {krajsi_od_5}, dolgih med 5 in 10 minut 84, dolgih med 10 in 15 minut 164, 
    dolgih med 15 in 20 minut 126, daljših od 20 minut pa 78.
    Pri stopnji značilnosti \\(\\alpha=0.10\\) testiramo ničelno domnevo (\\(H_0\\)),
    da je porazdelitev dolžine klica normalna z \\(\\mu={mu}\\) in \\(\\sigma={sigma}\\).
    </p>
 
    <p>Koliko je pričakovanih klicev s trajanjem v danem območju?</p>
    krajši od 5 min:&nbsp; {ans[0]}, med 5 in 10 min:&nbsp; {ans[1]}, med 10 in 15 min:&nbsp; {ans[2]}, 
    med 15 in 20 min:&nbsp; {ans[3]}, daljši od 20 min:&nbsp; {ans[4]}
 
    <p>Vrednost testne statistike je:&nbsp; {ans[5]}</p>
 
    <p>\\(p\\)-vrednost za testno statistiko je:&nbsp; {ans[6]}</p>
 
    <p>Rezultat testa je:
    {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>
    """
     
    ans=[]
    
    krajsi_od_5=st_klicev-84-164-126-78
    
    sredine_casovnih_intervalov=[2.5,7.5,12.5,17.5,22.5]
    opazene=[krajsi_od_5,84,164,126,78]
    print("Opažene frekvence so {}.".format(opazene))
    
    verjetnosti=[]
    for i in range(5):
        verjetnosti.append(opazene[i]/st_klicev)
    print("Opažene verjetnosti so {}.".format(verjetnosti))
    
    porazd = stats.rv_discrete(name='porazd', values=(sredine_casovnih_intervalov, verjetnosti))
    
    mu=porazd.mean()
    mu=round(mu,2)
    sigma=porazd.std()
    sigma=round(sigma,2)
    print("Vzorčno povprečje je {0}. Vzorčni odklon je {1}".format(mu,sigma))
    
    pricakovane=[]
    pric_verj=[]
    pric_verj.append(norm.cdf((5-mu)/sigma))
    ans.append(st_klicev*pric_verj[0])
    pricakovane.append(st_klicev*pric_verj[0])
    for i in range(4):
        verj=norm.cdf((sredine_casovnih_intervalov[i+1]+2.5-mu)/sigma)-sum(pric_verj)
        if i==3:
            verj=1-sum(pric_verj)
        pric_verj.append(verj)
        ans.append(st_klicev*pric_verj[i+1])
        pricakovane.append(st_klicev*pric_verj[i+1])
    print("Pričakovane verjetnosti so {}".format(pric_verj))
    print(sum(pric_verj))
    print("Pričakovane frekvence so {}".format(pricakovane))
 
    TS=0
    for i in range(5):
        TS+=(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)
 
    p_vrednost=chi2.sf(TS,4)
    ans.append(p_vrednost)
    print("Testna statistika je {0}, p-vrednost pa {1}.".format(TS,p_vrednost))
     
    if p_vrednost<0.10:
        print("zavrnemo")
    else:
        print("ne zavrnemo")
        
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    ans[6]="{{1:NUMERICAL:={}:0.001}}".format(p_vrednost)
    print(ans)
    cloze = text.format(st_klicev=st_klicev,krajsi_od_5=krajsi_od_5,mu=mu,sigma=sigma,ans=ans)
    return cloze
     
 
 
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
 
questions = ""
count = 1
 
for st_klicev in range(501,506):
    text = naloga(st_klicev)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1
 
 
 
# moodle XML export
 
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
 
    </category>
  </question>
 
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
 
  {questions}
 
</quiz>
"""
# read the code
 
with open(sys.argv[0]) as fp:
    CODE = fp.read()
 
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
 
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)


# coding: utf-8

# In[10]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import sys
import base64
from scipy.stats import norm, t, chi2, binom,poisson
from math import sqrt, ceil
from random import randint
from numpy import std,mean
import random
 
NAME = "Testiranje hipotez {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 13. kviz (testiranje hipotez)/6. skupina"
FILE_NAME = "ovs_kviz_14_6"
 
def naloga(st_iger):
    text = """<p>
    V neki igri na srečo igralec trikrat meče kocko, njegov dobiček pa je odvisen od števila vrženih šestic.
    Na podlagi vzorca {st_iger} iger pri stopnji značilnosti \\(\\alpha=0.05\\) testiramo ničelno hipotezo, da je 
    igralna kocka poštena. Na vzorcu nismo nobene šestice vrgli 52-krat, eno šestico smo vrgli 35-krat, dve šestici 10-krat,
    tri šestice pa {tri_sestice}-krat.
    </p>
 
    <p>Izračunajte pričakovane frekvence rezultatov:</p>
    <p>0 šestic:&nbsp; {ans[0]}, 1 šestica:&nbsp; {ans[1]}, 2 šestici:&nbsp; {ans[2]}, 3 šestice:&nbsp; {ans[3]}.</p>
 
    Ena od pričakovanih frekvenc je premajhna, da bi lahko naredili željeni test. Število šestic, pri katerem se to zgodi, 
    združi s številom, ki je za eno manjše, in nato naredi test za novo razdelitev rezultatov.
    
    <p>Vrednost testne statistike je:&nbsp; {ans[4]}</p>
 
    <p>\\(p\\)-vrednost za testno statistiko je:&nbsp; {ans[5]}</p>
 
    <p>Rezultat testa je:
    {{2:MC:~=\\(H_0\\) zavrnemo~ \\(H_0\\) ne zavrnemo, ker drži.~\\(H_0\\) ne zavrnemo, čeprav ne drži nujno.}}</p>
    """
     
    ans=[]
    pricakovane=[]
    tri_sestice=st_iger-52-35-10
    
    for i in range(4):
        f_i=binom.pmf(i,3,1/6)
        ans.append(st_iger*f_i)
        pricakovane.append(st_iger*f_i)
    print("Pričakovane frekvence so {}.".format(pricakovane))
 
    opazene=[52,35,10,tri_sestice]
    print("Opažene frekvence so {}.".format(opazene))
    
    opazene[2]+=opazene[3]
    pricakovane[2]+=pricakovane[3]
    
    print("Nove pričakovane frekvence so {}.".format(pricakovane[0:3]))
    print("Nove opažene frekvence so {}.".format(opazene[0:3]))
    
    TS=0
    for i in range(3):
        TS+=(opazene[i]-pricakovane[i])**2/pricakovane[i]
    ans.append(TS)
 
    p_vrednost=chi2.sf(TS,2)
    ans.append(p_vrednost)
    print("Vrednost testne statistike je {0}, p-vrednost pa je {1}.".format(TS,p_vrednost))
     
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    ans[5]="{{1:NUMERICAL:={}:0.001}}".format(p_vrednost)
    print(ans)
    cloze = text.format(st_iger=st_iger,tri_sestice=tri_sestice,ans=ans)
    return cloze
     
 
 
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""
 
questions = ""
count = 1
 
for st_iger in range(101,105):
    text = naloga(st_iger)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1
 
 
 
# moodle XML export
 
MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>
 
    </category>
  </question>
 
  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>
 
  {questions}
 
</quiz>
"""
# read the code
 
with open(sys.argv[0]) as fp:
    CODE = fp.read()
 
LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))
 
# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

