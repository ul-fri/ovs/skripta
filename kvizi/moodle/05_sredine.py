#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import hypergeom
import matplotlib


NAME = "Zvezne - splosno {}"

CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (matematično upanje)"
FILE_NAME = "vs_kviz_9_3"

text = """<p>Na srečelovu je {n} srečk, pri čemer je {m} dobitnih. Kupimo {k} srečk.
    Naj bo \\(X\\) slučajna spremenljivka, ki šteje število dobitnih srečk med kupljenimi srečkami.
    </p>

    <p>Spremenljivka X je porazdeljena:
    {{1:MC:geometrijsko~enakomerno~Poissonovo~normalno~=hipergeometrijsko~Pascalovo~binomsko}}. </p>

    <p>Koliko je verjetnost, da smo izžrebali natanko 3 dobitne srečke?&nbsp; {ans[0]} </p>

    <p>Koliko je pričakovano število dobitnih srečk?&nbsp; {ans[1]} </p>
    
    <p>Denimo, da je bila med izžrebanimi srečkami 1 dobitna. Zato se odločimo kupiti še {k} srečk
    izmed preostalih srečk. Koliko je pričakovano število dobitnih srečk pri drugem nakupu?&nbsp; {ans[2]} </p>
    """
def resitev(n,m,k):

    verj=hypergeom.pmf(3,n,m,k)
    upan=hypergeom.mean(n,m,k)
    upan2=hypergeom.mean(n-k,m-1,k)
    
    ans=[verj,upan,upan2]
    print(n,m,k,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]      
    
    cloze = text.format(n=n,m=m,k=k,ans=ans)
    return cloze

questions = ""
count = 1
for n in [100,200]:
    for m in [15,20]:
        for k in [10,15]:
            text = naloga(n,m,k)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import nbinom
import matplotlib


NAME = "Zvezne - splosno {}"

CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (matematično upanje)"
FILE_NAME = "vs_kviz_9_4"

text = """<p>Naftno podjetje je na podlagi preteklih opažanj ugotovilo, da naključno izbrana lokacija za naftno vrtino
    pripelje do nafte z verjetnostjo 0.2. Za izpolnitev dnevnega cilja mora podjetje izkopati {n} vrtin z nafto.
    Naj bo \\(X\\) slučajna spremenljivka, ki šteje število izvrtanih vrtin do izpolnitve dnevnega cilja.
    </p>

    <p>Spremenljivka \\(X\\) je porazdeljena:
    {{1:MC:geometrijsko~enakomerno~Poissonovo~normalno~hipergeometrijsko~=Pascalovo~binomsko}}. </p>

    <p>Koliko je verjetnost, da bodo izvrtali {m} vrtin?&nbsp; {ans[0]} </p>

    <p>Koliko je pričakovano število izvrtanih vrtin?&nbsp; {ans[1]} </p>
    
    <p>Naslednji dan podjetje dobi napravo za iskanje nafte, ki poveča verjetnost, da bo izbrana lokacija
    pripeljala do nafte na 0.3? Za koliko se zmanja pričakovano število vrtin za izpolnitev
    dnevnega cilja v primerjavi s prvim dnevom?&nbsp; {ans[2]} </p>
    """
def resitev(n,m):

    verj=nbinom.pmf(m-n,n,0.2)
    upan=nbinom.mean(n,0.2)+n
    upan2=nbinom.mean(n,0.3)+n
    razlika=upan-upan2
    
    ans=[verj,upan,razlika]
    print(n,m,ans)
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]      
    
    cloze = text.format(n=n,m=m,ans=ans)
    return cloze

questions = ""
count = 1
for n in [7,8]:
    for m in [31,35]:
            text = naloga(n,m)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import expon
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)"
FILE_NAME = "vs_kviz_9_5"

text = """<p>Število pristankov letal na letalski stezi sledi Poissonovi porazdelitvi, pri čemer v
    povprečju pristane {n} letal v eni uri.</p>

    <p>Koliko je verjetnost, da bo na stezi v naslednjih {m} minutah pristalo letalo?&nbsp; {ans[0]} </p>

    <p>Denimo, da že pol ure na stezi ni pristalo nobeno letalo. Koliko je verjetnost, da bo v naslednjih
    {m} minutah pristalo naslednje letalo?&nbsp; {ans[1]} </p>

    <p>Naposled je letalo le pristalo na stezi.
    Koliko časa (v minutah) pričakujemo, da bo minilo do
    pristanka naslednjega letala?&nbsp; {ans[2]}
    Koliko časa (v minutah) pa moramo čakati, da bo z 90% verjetnostjo na stezi pristalo letalo.&nbsp; {ans[3]}
    </p>
    """
def resitev(n,m):

    verj=expon.cdf(1.0/60*m,0,1.0/n)
    upan=1.0/n*60
    t=60*expon.isf(0.1,0,1.0/n)
    
    ans=[verj,verj,upan,t]
    print(n,m,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]      
    
    cloze = text.format(n=n,m=m,ans=ans)
    return cloze

questions = ""
count = 1
for n in [5,6,10]:
    for m in [10,15,20]:
            text = naloga(n,m)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import expon
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/6. skupina"
FILE_NAME = "vs_kviz_9_6"

text = """<p>Naj bosta dani neodvisni normalno porazdeljeni
    slučajni spremenljivki \\(X\\sim N({mu1},5)\\),
    \\(Y\\sim N({mu2},4)\\) in eksponentno porazdeljena spremeljivka
    \\(Z\\sim Exp({lam})\\).</p>

    <p>Izračunaj matematično upanje naslednjih spremenljivk: {upanja}</p>

    Naj bo \\(W\\) spremenljivka z največjim upanjem iz prejšnjega vprašanja.
    
    <p>Koliko je \\( P(1\\leq W)\\)?&nbsp; {ans[0]} </p>
    
    <p>Določi \\(T\\) tako, da bo veljalo
    \\( P(X\\leq {mu1})=P(W\\leq T) \\)?&nbsp; {ans[1]} </p>

    """
def resitev(mu1,mu2,lam):

    mu=mu1+2.0*1/lam
    up=[('\\(X\\):',mu1),('\\(Y\\):',mu2),('\\(Z\\):',1.0/lam),('\\(X-Y\\):',mu1-mu2),
        ('\\(X+2Z\\)',mu)]
    upanja= ", ".join("{} {{1:NUMERICAL:={}:0.0001}}".format(p[0],p[1]) for p in up)

    verj=expon.sf(1,0,1.0/lam)

    T=expon.isf(0.5,0,1.0/lam)
    
    ans = [verj,T]
    print(mu1,mu2,lam,ans)
    ans = ["{{5:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(mu1=mu1, mu2=mu2,lam=lam,upanja=upanja,mu=mu,ans=ans)
    return cloze

questions = ""
count = 1
for mu1 in [-40,-30]:
    for mu2 in [-10,-5]:
            for lam in [1.5,2.5,3.5]:
                text = naloga(mu1,mu2,lam)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import hypergeom
import matplotlib


NAME = "Disperzija {}"

CATEGORY = "Kvizi za oceno/Kviz, 11. teden, 10. kviz (disperzija)"
FILE_NAME = "vs_kviz_11_1_1"

text = """<p>Na srečelovu je {n} srečk, pri čemer je {m} dobitnih. Kupimo {k} srečk.
    Naj bo \\(X\\) slučajna spremenljivka, ki šteje število dobitnih srečk med kupljenimi srečkami.
    </p>

    <p>Koliko je pričakovano število dobitnih srečk?&nbsp; {ans[0]} Koliko je disperzija spremenljivke $X$?&nbsp; {ans[1]} </p>

    <p>Denimo, da moramo za vsak dobitek plačati davek. Če smo izvleki $k$ dobitnih srečk, moramo plačati
    $k^2$ evrov davka. Koliko je pričakovan davek v evrih?&nbsp; {ans[2]} </p>
    
    <p>Denimo, da vsaka srečka stane 1 evro, vsaka dobitna pa prinese $t$ evrov dobitka. Koliko mora biti $t$,
    da bo igra poštena (pričakovan dobitek mora biti enak pričakovani vsoti kupnine srečk in davka)? &nbsp; {ans[3]} </p>
    """
def resitev(n,m,k):

    upan=hypergeom.mean(n,m,k)
    disp=hypergeom.var(n,m,k)
    c=disp+upan**2
    t=(k+c)/upan

    ans=[upan,disp,c,t]
    print(n,m,k,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]      
    
    cloze = text.format(n=n,m=m,k=k,ans=ans)
    return cloze

questions = ""
count = 1
for n in [100,200]:
    for m in [15,20]:
        for k in [10,15]:
            text = naloga(n,m,k)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import poisson
import matplotlib


NAME = "Disperzija"
CATEGORY = "Kvizi za oceno/Kviz, 11. teden, 10. kviz (disperzija)"
FILE_NAME = "vs_kviz_11_2_1"

text = """<p>Število letal, ki prispejo na letališče z zamudo, sledi Poissonovi porazdelitvi, pri čemer v
    povprečju zamudi {n} letal <b>na dan</b>. Naj bo \\(X\\) slučajna spremenljivka, ki v neki <b>opazovani uri</b> dneva meri število letal, ki zamudijo.</p>

    <p>Koliko je pričakovana vrednost spremenljivke $X$?&nbsp; {ans[0]}  Koliko pa njena disperzija?&nbsp; {ans[1]} </p>

    <p>Koliko je verjetnost, da v <b>opazovani uri</b> zamudijo vsaj 3 letala?&nbsp; {ans[3]} </p>

    <p>Denimo, da so stroški letalske družbe, nastali zaradi zamud v <b>opazovani uri</b>, enaki $1000\\cdot k^2$ evrov, kjer je $k$ število zamud.
    Koliko so pričakovani stroški (v evrih)?&nbsp; {ans[2]}
    </p>
   
    """
def resitev(n):

    upan=poisson.mean(1.0*n/24)
    disp=poisson.var(1.0*n/24)
    stroski=1000*(disp+upan**2)
    vsajtri=poisson.sf(2,1.0*n/24)

    print(n,upan,disp,vsajtri,stroski)     

    ans=[upan,disp,stroski,vsajtri]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]      
    
    cloze = text.format(n=n,ans=ans)
    return cloze

questions = ""
count = 1
for n in [90,100,110,120,130]:
        text = naloga(n)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import nbinom
import matplotlib


NAME = "Disperzija {}"

CATEGORY = "Kvizi za oceno/Kviz, 11. teden, 10. kviz (disperzija"
FILE_NAME = "vs_kviz_11_3_1"

text = """<p>Naftno podjetje je na podlagi preteklih opažanj ugotovilo, da naključno izbrana lokacija za naftno vrtino
    pripelje do nafte z verjetnostjo 0.2. Za izpolnitev dnevnega cilja mora podjetje izkopati {n} vrtin z nafto.
    Naj bo \\(X\\) slučajna spremenljivka, ki šteje število izvrtanih vrtin do izpolnitve dnevnega cilja.
    </p>

    <p>Koliko je pričakovano število izvrtanih vrtin?&nbsp; {ans[0]} Koliko je disperzija spremenljivke $X$?&nbsp; {ans[1]} </p>

    <p>Vsaka izvrtana vrtina predstavlja 1000 evrov stroška za podjetje.
    Predpostavimo, da je v vsaki vrtini z nafto ista količina nafte.
    Vsaj za koliko evrov nafte mora biti v vsaki vrtini z nafto,
    da podjetje v povprečju ne bo delalo izgube?&nbsp; {ans[2]} </p>
    Odgovorite na isto vprašanje še v primeru, ko mora podjetje
    poleg stroškov izkopa plačati še okoljske dajatve, ki so v primeru $k$ izkopanih vrtin enake $100 \\cdot k^2$ evrov? &nbsp; {ans[3]} </p>
    """
def resitev(n):

    upan=nbinom.mean(n,0.2)+n
    disp=nbinom.var(n,0.2)
    t=1000.0*upan/n
    u=(1000.0*upan+100*(disp+upan**2))/n
    
    ans=[upan,disp,t,u]
    print(n,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]      
    
    cloze = text.format(n=n,ans=ans)
    return cloze


questions = ""
count = 1
for n in [6,7,8,9]:
        text = naloga(n)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import expon, uniform
from scipy.integrate import quad
from numpy import inf
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 11. teden, 10. kviz (disperzija)/4. skupina"
FILE_NAME = "vs_kviz_11_4_1"

    c=ver/5
    d=1-ver
    upan=c*5*uniform.mean(-5,5)+d*expon.mean(0,1.0/lam)
    disp=quad(lambda x:c*x**2,-5,0)[0]+quad(lambda x:x**2*d*lam*exp(-lam*x),0,inf)[0]-upan**2
    
    ans = [c,d,upan,disp]
    print(ver,lam,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
        
text = """<p>Zvezna slučajna spremenljivka $X$ ima gostoto verjetnosti
    $$p_{X}(x)=\\left\{\\begin{array}{cc}
    c & x\\in [-5,0)\\\\
    d\\cdot (%(lam).1f \\cdot e^{-%(lam).1f\\cdot x}) & x\\in [0,\\infty) \\\\
    0 & \\text{sicer}
    \\end{array} \\right.,$$
    kjer sta $c$ in $d$ konstanti. 
    
    Naj velja $P(X\\leq 0)=%(ver).1f.$</p>

    <p>Koliko sta konstanti $c$ in $d$? &nbsp; $c$: %(ans[0])s $d$: %(ans[1])s

    <p>Koliko je \\( E(X) \\)</b>?&nbsp; %(ans[2])s </p>
    
    <p>Koliko je $D(X)$</b>?&nbsp;  %(ans[3])s </p>

    """ %{ "lam": lam, "ver": ver, "ans[0]": ans[0], "ans[1]": ans[1], "ans[2]": ans[2], "ans[3]": ans[3] }
def resitev(ver,lam):

    return text

questions = ""
count = 1
for ver in [0.6,0.7]:
    for lam in [0.5,1.5,2.5]:
        text = naloga(ver,lam)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import expon, uniform
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 11. teden, 10. kviz (disperzija)/5. skupina"
FILE_NAME = "vs_kviz_11_5_1"


    s=n+1
    m=sum((n+1-i)**2 for i in range(1,n+1))+10
    upan=sum(1.0*(n+1-i)**2/m*i for i in range(1,n+1))
    disp=sum(1.0*(n+1-i)**2/m*i**2 for i in range(1,n+1))-upan**2
    vlozek=int(upan*0.5)
    k=int(10.0/(upan-vlozek))+1
    zas=upan-vlozek-1.0/20*(disp+upan**2)
    l=int(10.0/zas)+1

    verj=sum(1.0*(n+1-i)**2/m for i in range(1,n+1))
          
          
    ans = [upan,disp,k,l]
    print(n,vlozek,zas,verj,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
        
    
    text="""<p>Igramo igro na srečo, za katero moramo plačati %(vlozek)d evro/a. Naš dobitek pri igri je lahko $k\\in \\left\{1,2,\\ldots,%(n)d\\right\\}$ evrov in
    sicer z verjetnostjo $P(X=k)=\\frac{(%(s)d-k)^2}{%(m)d}$, sicer pa ostanemo praznih rok.

    <p>Koliko je pričakovan dobitek? %(ans[0])s Koliko je disperzija dobitka? %(ans[1])s </p>

    <p>Koliko iger bo po pričakovanjih potrebno odigrati, da bo naš zaslužek vsaj 10 evrov? (Pri tem je zaslužek pri eni igri enak razliki med dobitkom in plačilom za igro.) %(ans[2])s
    Koliko iger pa za isti zaslužek v državi, kjer moramo poleg plačila za igro, v primeru dobitka $k$ evrov, plačati še $\\frac{k^2}{20}$ evrov davka? %(ans[3])s </p>

    """ %{"s":s, "n": n, "m": m, "vlozek": vlozek, "ans[0]": ans[0], "ans[1]": ans[1], "ans[2]": ans[2],"ans[3]": ans[3]}
def resitev(n):

    return text

questions = ""
count = 1
for n in [8,10,13,17]:
text = naloga(n)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.integrate import quad
import matplotlib


NAME = "Disperzija {}"
CATEGORY = "Kvizi za oceno/Kviz, 11. teden, 10. kviz (disperzija)/6. skupina"
FILE_NAME = "vs_kviz_11_6_1"


    funx=[["\\frac{1}{8}(x^2+3x+2)","\\frac{1}{4}x+\\frac{1}{2}"],["\\frac{1}{4}(x+1)","\\frac{1}{4}(x^2-2x+4)"],["\\frac{1}{4}(x+1)","\\frac{1}{2}x"],["\\frac{1}{8}(x^2+3x+1)","\\frac{1}{8}(3x^2-6x+8)"]]
    fun1=[[lambda x:1.0/8*(2*x+3),lambda x:1.0/4], [lambda x:1.0/4,lambda x:1.0/4*(2*x-2)], [lambda x:1.0/4,lambda x:1.0/2], [lambda x:1.0/8*(2*x+3),lambda x:1.0/8*(6*x-6)]  ]
    
    upX=quad(lambda x: fun1[i][0](x)*x, 0,1)[0]+quad(lambda x: fun1[i][1](x)*x, 1,2)[0]
    dispX=quad(lambda x: fun1[i][0](x)*x**2, 0,1)[0]+quad(lambda x: fun1[i][1](x)*x**2, 1,2)[0]-upX**2

    y=[0,1,2]
    verj=[1.0/3,1.0/2-1.0/3,1-1.0/2]

    upY=0
    for j in range(0,3):
        upY=upY+y[j]*verj[j]
    dispY=0
    for j in range(0,3):
        dispY=dispY+y[j]**2*verj[j]
    dispY=dispY-upY**2

    komb=4*(dispX+upX**2)+12*upX*upY+9*(dispY+upY**2)
          
    ans = [upX,dispX,upY,dispY,komb]
    print(i,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
        
    
    text="""<p>Naj bosta $X$ in $Y$ neodvisni s.s. s porazdelitvenima funkcijama
    $$F_X(x)=\\left\{\\begin{array}{cc}
        0 & x\\leq 0\\\\
        %(funx[i][0])s  & x\\in (0,1]\\\\
        %(funx[i][1])s  & x\\in (1,2]\\\\
        1 & x\in (2,\\infty)
    \\end{array}\\right.,\quad
    F_Y(y)=\\left\{\\begin{array}{cc}
        0 & y\\in (-\\infty,0)\\\\
        \\frac{1}{3} & y\\in [0,1)\\\\
        \\frac{1}{2} & y\\in [1,2)\\\\
        1 & y\\in [2,\\infty)
    \\end{array}\\right.,
    $$</p>
    
    <p>Izračunajte naslednje količine:</p>

    <p>
    $E(X):$ %(ans[0])s, $D(X):$ %(ans[1])s, $E(Y):$ %(ans[2])s, $D(Y):$ %(ans[3])s, $E((2X+3Y)^2):$ %(ans[4])s,
    </p>
    """ %{"funx[i][0]": funx[i][0], "funx[i][1]": funx[i][1], "ans[0]": ans[0], "ans[1]": ans[1], "ans[2]": ans[2], "ans[3]": ans[3], "ans[4]": ans[4]}
def resitev(i):

    return text

questions = ""
count = 1
for i in range(0,4):
text = naloga(i)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
QUIZ.append(QuestionSet(text, parametri, resitev18, name="kroglice"))

with open(POGLAVJE.replace(" ","_")+".xml", "w") as file_pointer:
    file_pointer.write(str(QUIZ))
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())
os.system("latexmk -pdf {}".format("vs_kvizi.tex"))
