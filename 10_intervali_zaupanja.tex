\chapter{Intervali zaupanja}


% =======
% TEORIJA
% =======


\begin{center}\begin{fminipage}{14cm}
    \textbf{Interval zaupanja za neznani parameter porazdelitve}
    Naj bo $\theta$ neznani parameter porazdelitve slučajne spremenljivke $X$ in $(X_1,X_2,\ldots , X_n)$ enostavni slučajni vzorec.
    Iščemo interval vrednosti, v katerem se, z veliko verjetnostjo, nahaja neznani parameter $\theta$.\\
    Na osnovi vzorca se definirata statistiki (funkciji vzorca) $L$ in $U$ tako da velja
    \begin{equation*}
      P(L\leq \theta \leq U)=1-\alpha.
    \end{equation*}
    Potem rečemo, da je $I_{\theta}=[L,~U]$ interval zaupanja za neznani parameter $\theta$ s \emph{stopnjo zaupanja} $1-\alpha$. Število $\alpha$ se imenuje \emph{stopnja tveganja}.

    Običajno se računa $90\%$, $95\%$ ali $99\%$ interval zaupanja ($1-\alpha$ je enako $0.9$, $0.95$ ali $0.99$).
  \end{fminipage}\end{center}


% =======
% TEORIJA
% =======


\begin{center}\begin{fminipage}{14cm}
    \textbf{Interval zaupanja za pričakovano vrednost $\mu$}

    Naj bo $X$ normalno porazdeljena slučajna spremenljivka, $X\sim N(\mu,\sigma)$ in naj bo
    $(X_1,X_2,\ldots, X_n)$ enostavni slučajni vzorec.

    Pričakovana vrednost $\mu$ se ocenjuje z vzorčnim povprečjem $\overline{X}=\frac{1}{n}\sum_{i=1}^n X_i$.

    \textbf{1. možnost: standardni odklon $\sigma$ je znan}
    Interval zaupanja za $\mu$ s stopnjo zaupanja $1-\alpha$ je enak
    \begin{equation*}
      I_{\mu}=\left[\overline{X}-c\frac{\sigma}{\sqrt{n}},\; \overline{X}+c\frac{\sigma}{\sqrt{n}}\right],
    \end{equation*}
    kjer je $c=\Fi^{-1}\left(1-\frac{\alpha}{2}\right)$ kvantil standardne normalne porazdelitve.

    \textbf{2. možnost: standardni odklon $\sigma$ ni znan}
    Standardni odklon se ocenjuje s popravljenim vzorčnim standardnim odklonom $S=\sqrt{\frac{1}{n-1}\sum_{i=1}^n(X_i-\overline{X})^2}$.

    Interval zaupanja za $\mu$ s stopnjo zaupanja $1-\alpha$ je enak
    \begin{equation*}
      I_{\mu}=\left[\overline{X}-c\frac{S}{\sqrt{n}},\; \overline{X}+c\frac{S}{\sqrt{n}}\right],
    \end{equation*}
    kjer je $c=t_{n-1;1-\frac{\alpha}{2}}$ kvantil Studentove porazdelitve z $n-1$ prostostnimi stopnjami.
  \end{fminipage}\end{center}

\begin{center}\begin{fminipage}{14cm}
    \textbf{Opomba} Opažene vrednosti vzorca označimo z $(x_1,x_2,\ldots x_n)$, dobljene vrednosti vzorčnega povprečja z $\overline{x}$ in
    vzorčnega standardnega odklona s $s$. Na ta način razlikujemo, kaj je slučajna spremenljivka (velika črka) in kaj je njena vrednost (mala črka).
  \end{fminipage}\end{center}


\begin{naloga}[vaje]{iz_1}[resena]
  Slučajna spremenljivka $X$ je porazdeljena normalno, $X\!\sim\! N(\mu,5)$. Dobili smo slučajni vzorec $101, 91, 93, 103, 91, 101, 103, 95, 95$.
  \begin{enumerate}
    \item Določi $95$\% interval zaupanja za pričakovano vrednost $\mu$.
    \item Določi stopnjo zaupanja, pri kateri bo interval zaupanja za $\mu$ dolg 10.
  \end{enumerate}
  \begin{rezultat}
    a. $\overline{x}=97$, $I_{\mu}=[97-3.27,~97+3.27]=[93.73,~ 100.27]$
    b. $d=2c\frac{\sigma}{\sqrt{n}}=10$, $1-\alpha=0.9974$.
  \end{rezultat}
  \begin{resitev}
    \begin{enumerate}
      \item Standardni odklon slučajne spremenljivke $X$ je enak $\sigma=5$, stopnja zaupanja $1-\alpha=0.95$ (oziroma stopnja tveganja $\alpha=0.05$) in velikost vzorca $n=9$. Izračunajmo vzorčno povprečje
            \begin{equation*}
              \overline{x}=\frac{101+91+93+103+91+101+103+95+95}{9}=\frac{873}{9}=97.
            \end{equation*}
            Interval zaupanja za $\mu$ je $I_{\mu}=\left[\overline{x}-c\frac{\sigma}{\sqrt{n}},\; \overline{x}+c\frac{\sigma}{\sqrt{n}}\right]$, kjer je
            \begin{equation*}
              c=\Fi^{-1}\left(1-\frac{0.05}{2}\right)=\Fi^{-1}(0.975)=1.96
            \end{equation*}
            (v tabeli normalne porazdelitve iščemo vrednost $x$, ki ji ustreza verjetnost $0.975$).

            Zamenjamo vse dobljene vrednosti v izraz za interval zaupanja za pričakovano vrednost $\mu$:
            \begin{equation*}
              I_{\mu}=\left[97-1.96\frac{5}{\sqrt{9}},\; 97+1.96\frac{5}{\sqrt{9}}\right]=[97-3.27,~97+3.27]=[93.73,~ 100.27]
            \end{equation*}
      \item Dolžina intervala zaupanja za $\mu$ je enaka
            \begin{equation*}
              d=\overline{x}+c\frac{\sigma}{\sqrt{n}}-\left(\overline{x}-c\frac{\sigma}{\sqrt{n}}\right)=2c\frac{\sigma}{\sqrt{n}}.
            \end{equation*}
            Zamenjamo vrednosti za $\sigma$ in $n$ v zgornji izraz za dolžino. Dobimo
            \begin{equation*}
              d=2c\frac{5}{3}=10.
            \end{equation*}
            oziroma $c=3$. Izkoristimo, da je $c=\Fi^{-1}\left(1-\frac{\alpha}{2}\right)$.  Dobimo
            \begin{align*}
               & \Fi^{-1}\left(1-\frac{\alpha}{2}\right)=3, \\
               & \implies 1-\frac{\alpha}{2}=\Fi(3)=0.9987.
            \end{align*}
            Potem je stopnja tveganja enaka $\alpha=0.0026$, oziroma stopnja zaupanja
            $1-\alpha=0.9974$.
    \end{enumerate}
  \end{resitev}
\end{naloga}

%\begin{naloga}{iz_2}
%Določi interval zaupanja za $\mu$ pri $\gamma\!=\!0.99$, če je $X\!\sim\!\mathrm{N}(\mu,3)$. Pri vzorcu s $100$ elementi smo izracunali povprečje $\overline{x}=5$.
%\begin{rezultat}
%\end{rezultat}
%\begin{resitev}
%\end{resitev}
%\end{naloga}

\begin{naloga}{iz_2}
  Signal intenzitete $\mu$ je poslan z lokacije A. Na lokaciji $B$ se beleži sprejet signal. Zaradi šumenja signal zaznamo z naključno napako.
  Intenziteta signala na lokaciji $B$ je normalno  porazdeljena slučajna spremenljivka s pričakovano vrednostjo $\mu$ in standardnim odklonom 3.
  Da bi zmanjšali napako, isti signal neodvisno beležimo 10-krat. Dobili smo naslednje vrednosti intenzitete signala na lokaciji B:
  $17, 21, 20, 18, 19, 22, 20, 21, 16, 19$. Določi $95\%$ interval zaupanja za pričakovano vrednost $\mu$.
  \begin{rezultat}
    $\overline{x}=19.3$, $I_{\mu}=[19.3-1.86,~ 19.3+1.86]=[17.44, ~ 21.16]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}

\begin{naloga}[vaje]{iz_3}[resena]
  Za $X\!\sim\! N(\mu,2)$, določi najmanjšo velikost vzorca, pri kateri bo $99\%$ interval zaupanja za $\mu$ dolg največ $0.1$.
  \begin{rezultat}
    $d=2c\frac{\sigma}{\sqrt{n}}\leq 0.1$, $n=10651$.
  \end{rezultat}
  \begin{resitev}
    Standardni odklon slučajne spremenljivke $X$ je enak $\sigma=2$ in stopnja zaupanja $1-\alpha=0.99$
    (oziroma stopnja tveganja $\alpha=0.01$).
    Interval zaupanja za pričakovano vrednost $\mu$ je $I_{\mu}=\left[\overline{X}-c\frac{\sigma}{\sqrt{n}},\; \overline{X}+c\frac{\sigma}{\sqrt{n}}\right]$.
    Dolžina intervala zaupanja za $\mu$ je enaka
    \begin{equation*}
      d=\overline{X}+c\frac{\sigma}{\sqrt{n}}-\left(\overline{X}-c\frac{\sigma}{\sqrt{n}}\right)=2c\frac{\sigma}{\sqrt{n}},
    \end{equation*}
    in dano je $d\leq 0.1$.

    V tabeli normalne porazdelitve najdemo $c=\Fi^{-1}\left(1-\frac{0.01}{2}\right)=\Fi^{-1}(0.995)=2.58$. Zamenjamo $c$ in $\sigma$ v izraz za $d$:
    \begin{equation*}
      2\cdot 2.58\frac{2}{\sqrt{n}}\leq 0.1.
    \end{equation*}
    Dobimo $\sqrt{n}\geq\!103.2$, oziroma $n\!\geq\!10650.2$. Najmanjši število $n$, ki zadovoljuje to neenakost, je $n=10651$.
  \end{resitev}
\end{naloga}





\begin{naloga}{iz_4}
  Iz prejšnjih izkušenj vemo, da je teža lososov, gojenih v komercialni ribogojnici
  normalno porazdeljena s povprečjem, ki varira od sezone do sezone in konstantnim standardnim odklonom 140~g. Kako veliki vzorec potrebujemo, če želimo biti $90\%$ prepričani, da je ocena povprečne teže lososov natančna na $\pm 50$~g?
  \begin{rezultat}
    $\epsilon=c\frac{\sigma}{\sqrt{n}}\leq 50$, $c=1.645$, $n=22$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}


\begin{naloga}{iz_5}
  Standardni odklon življenjske dobe določene vrste žarnic je
  enak $120$ ur. Na naključnem vzorcu $100$ žarnic smo izračunali povprečje $1350$ ur. Določi $95\%$ interval zaupanja za povprečno življensko dobo $\mu$.
  \begin{rezultat}
    $I_{\mu}=\left[1350-1.96\frac{120}{\sqrt{100}}, ~ 1350+1.96\frac{120}{\sqrt{100}}\right]=[1350-23.52, ~ 1350+23.52]=[1326.48,~ 1373.52]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}




%\begin{naloga}[vaje]{iz_4}[resena]
%Za $X\!\sim\!\mathrm{N}(\mu,\sigma)$ imamo vzorec $101,91,93,103,91,101,103,95,95$. Doloci 95\% interval zaupanja za $\mu$.
%\begin{rezultat}
%\end{rezultat}
%\begin{resitev}
%$[93.15,100.85]$
%\end{resitev}
%\end{naloga}

\begin{naloga}[vaje]{iz_6}[resena]
  Slučajna spremenljivka $X$ je porazdeljena normalno, $X\!\sim\!\mathrm{N}(\mu,\sigma)$. Dobili smo slučajni vzorec $-0.3,1.4,0.5,0.9,1.2,-0.4,0.2,-0.2,1.5,0.6,-0.4, 1$.
  \begin{enumerate}
    \item Izračunaj vzorčno povprečje in popravljeni vzorčni standardni odklon.
    \item Izračunaj $95\%$ interval zaupanja za pričakovano vrednost $\mu$.
  \end{enumerate}
  \begin{rezultat}
    a. $\overline{x}=0.5$, $s=0.71$,
    b. $I_{\mu}=[0.5-0.45,~ 0.5+0.45]=[0.05,~ 0.95]$.
  \end{rezultat}
  \begin{resitev}
    \begin{enumerate}
      \item Vzorec je velikosti $n=12$. Izračunajmo vzorčno povprečje in popravljeno vzorčno varianco
            \begin{align*}
               & \overline{x}=\frac{6}{12}=0.5,                                                 \\
               & s^2=\frac{(-0.3-0.5)^2+(1.4-0.5)^2+\ldots (1-0.5)^2}{11}=\frac{5.56}{11}=0.51,
            \end{align*}
            Potem je popravljeni vzorčni standardni odklon  enak $s=\sqrt{s^2}=0.71$.
      \item Dana je stopnja zaupanja $1-\alpha=0.95$ (oziroma stopnja tveganja $\alpha=0.05$). Interval zaupanja za pričakovano vrednost $\mu$ s stopnjo zaupanja $95\%$ je enak
            \[
              I_{\mu}=\left[\overline{x}-c\frac{s}{\sqrt{n}},\; \overline{x}+c\frac{s}{\sqrt{n}}\right],
            \]
            kjer je
            \begin{equation*}
              c=t_{n-1; 1-\frac{0.05}{2}}=t_{11; 0.975}=2.2
            \end{equation*}
            (vrednost $c$ najdemo v tabeli kvantilov Studentove porazdelitve z $11$ prostostnimi stopnjami in za verjetnost $0.975$).
            Zamenjamo dobljene vrednosti v izraz za interval zaupanja za $\mu$:
            \begin{equation*}
              I_{\mu}=\left[0.5-2.2\frac{0.71}{\sqrt{12}}, 0.5+2.2\frac{0.71}{\sqrt{12}}\right]=[0.5-0.45,~ 0.5+0.45]=[0.05,~ 0.95].
            \end{equation*}
    \end{enumerate}
  \end{resitev}
\end{naloga}





%\begin{naloga}{iz_6}
%Naj ima slučajna spremenljivka $X$ na populaciji porazdelitev $\mathrm{N}(\mu,\sigma)$. Na vzorcu velikosti $n=15$ smo izračunali povprečje $\overline{x}=12.24$ in vzorčni odklon $s=0.6$. Določi interval zaupanja pri stopnji zaupanja $\gamma=0.95$.
%\begin{rezultat}
%$I_{\mu}=[12.24-0.332, ~ 12.24+0.332]=[11.908,~ 12.572]$.
%\end{rezultat}
%\begin{resitev}
%\end{resitev}
%\end{naloga}

\begin{naloga}{iz_7}
  Ameriška agencija za zaščito okolja je zaskrbljena zaradi količine polikloriranih bifenilov
  (PCB), ki so strupene kemikalije, v mleku doječih mater. Na vzorcu $50$ žensk so dobili
  povprečje $6$ in popravljeni standardni odklon $5$ količine PCB-jev, merjeno v delcih na milijon (ppm). Določi $99\%$ interval zaupanja
  za povprečno količino PCB-jev v mleku populacije doječih mater.
  \begin{rezultat}
    $I_{\mu}=\left[6-2.68\frac{5}{\sqrt{50}}, ~ 6+2.68\frac{5}{\sqrt{50}}\right]=[6-1.9, ~ 6+1.9]=[4.1,~ 7.9]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}

\begin{naloga}{iz_8}
  Zanima nas povprečna vsebnost nikotina $\mu$ v novi znamki cigaret.
  Naključno smo izbrali vzorec $40$ cigaret in izmerili vsebnost nikotina.
  Izračunali smo vzorčno povprečje $1.74$~mg in popravljeni vzorčni standardni odklon
  $0.7$~mg. Določi $95\%$ interval zaupanja za povprečno vsebnost nikotina.
  \begin{rezultat}
    $I_{\mu}=\left[1.74-2.02\frac{0.7}{\sqrt{40}}, ~ 1.74+2.02\frac{0.7}{\sqrt{40}}\right]=[1.74-0.224, ~ 1.74+0.224]=[1.516, ~1.964]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}

\begin{naloga}{iz_9}
  Ravnatelj šole želi določiti povprečno število dni odsotnosti dijakov s pouka prejšnjega leta. Namesto, da bi pogledal podatke
  za vse dijake, se je odločil naključno izbrati vzorec $50$ imen dijakov in zabeležiti njihovo število dni odsotnosti. Izračunal je, da je vzorčno povprečje $8.4$ dni in popravljeni vzorčni standardni odklon $5.1$ dni. Določi $95\%$ interval zaupanja za povprečno število
  dni odsotnosti vseh dijakov šole.
  \begin{rezultat}
    $I_{\mu}=\left[8.4-2.01\frac{5.1}{\sqrt{50}}, ~ 8.4+2.01\frac{5.1}{\sqrt{50}}\right]=[8.4-1.45, ~ 8.4+1.45]=[6.95, ~9.85]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}


%\begin{naloga}{iz_10}
%Želimo najeti stanovanje. Povprečna mesečna najemnina za slučajni vzorec $10$ stanovanj, ki jih oglašujejo v časopisu, je $540$ evrov. Predpostavi, da je odklon $\sigma$ enak $80$ evrov.
%\begin{enumerate}
%\item Določi interval zaupanja za povprečno mesečno najemnino pri $\gamma\!=\!0.95$.
%\item Če želimo, da ima interval pri tem $\gamma$ dolžino $\leq40$evrov, kako velik mora biti vzorec?
%\end{enumerate}
%\begin{rezultat}
%\end{rezultat}
%\begin{resitev}\end{resitev}
%\end{naloga}






%\begin{naloga}{iz_7}
%Statistični urad želi dolociti povprečen čas študija slovenskih študentov. Anketirajo vzorec 10 študentov in dobijo podatke o trajanju študija (v letih): $5,6,10,10,9,7,8,8,9,8$.
%\begin{enumerate}
%\item Izračunaj vzorčno povprečje $\overline{x}$ in vzorčni odklon $s$.
%\item S stopnjo zaupanja $\gamma\!=\!0.95$ doloci interval zaupanja za povprečno dolžino študija.
%\end{enumerate}
%\begin{rezultat}
%\end{rezultat}
%\begin{resitev}
%\begin{enumerate}
%\item
%\item
%\end{enumerate}
%\end{resitev}
%\end{naloga}



%\begin{naloga}{iz_8}
%Dan je sledeč vzorec vrednosti za slučajno spremenljivko $X$: $2.0, 5.0, -2.0, 3.5, 0.5,  -6.0$. Izračunaj interval zaupanja za $E(X)$ pri stopnji zaupanja $\gamma = 90\%$.
%\begin{resitev}
%\end{resitev}
%\end{naloga}



% =======
% TEORIJA
% =======
\begin{center}\begin{fminipage}{14cm}
    \textbf{Interval zaupanja za standardni odklon $\sigma$ (pričakovana vrednost $\mu$ ni znana)}
    Naj bo $X$ normalno porazdeljena slučajna spremenljivka, $X\sim N(\mu,\sigma)$ in naj bo
    $(X_1,X_2,\ldots, X_n)$ enostavni slučajni vzorec.

    Standardni odklon se ocenjuje s popravljenim vzorčnim standardnim odklonom $S=\sqrt{\frac{1}{n-1}\sum_{i=1}^n(X_i-\overline{X})^2}$. Interval zaupanja za $\sigma$ s stopnjo zaupanja
    $1-\alpha$ je enak
    \begin{equation*}
      I_{\sigma}=\left[S\sqrt{\frac{n-1}{c_2}},\; S\sqrt{\frac{n-1}{c_1}}\right],
    \end{equation*}
    kjer sta $c_1=\chi^2_{n-1; \frac{\alpha}{2}}$, $c_2=\chi^2_{n-1; 1-\frac{\alpha}{2}}$ kvantila $\chi^2$ porazdelitve z $n-1$ prostostnimi stopnjami.
  \end{fminipage}\end{center}


\begin{naloga}[vaje]{iz_10}[resena]
  Slučajna spremenljivka $X$ je porazdeljena normalno, $X\sim N(\mu,\sigma)$. Izbrali smo slučajni vzorec velikosti $n=20$ in smo izračunali popravljeni vzorčni standardni odklon $s=0.7$. Poišči interval zaupanja za standardni odklon $\sigma$ pri stopnji zaupanja $0.95$, nato pa še pri stopnji zaupanja $0.99$. Kaj opaziš?
  \begin{rezultat}
    $95\%$ interval zaupanja: $I_{\sigma}=[0.53,1.02]$, $99\%$ interval zaupanja:  $I_{\sigma}=[0.49,1.17]$. Za višjo stopnjo zaupanja se dobi širši interval zaupanja.
  \end{rezultat}
  \begin{resitev}
    Vzorec je velikosti $n=20$, popravljeni vzorčni standardni odklon pa $s=0.7$. Interval zaupanja za standardni odklon $\sigma$ je enak
    $I_{\sigma}=\left[s\sqrt{\frac{n-1}{c_2}},\; s\sqrt{\frac{n-1}{c_1}}\right]$. Za stopnjo zaupanja $1-\alpha=0.95$ (oziroma stopnjo tveganja $\alpha=0.05$), dobimo vrednosti $c_1$ in $c_2$
    \begin{align*}
       & c_1=\chi^2_{n-1; \frac{0.05}{2}}=\chi^2_{19;0.025}=8.91,  \\
       & c_2=\chi^2_{n-1; 1-\frac{0.05}{2}}=\chi^2_{19;0.975}=32.9
    \end{align*}
    (vrednost $c_1$ najdemo v tabeli kvantilov $\chi^2$ porazdelitve z $19$ prostostnimi stopnjami in za verjetnost $0.025$, vrednost $c_2$ pa za $19$ prostostnih stopenj in za verjetnost $0.975$).

    Zamenjamo dobljene vrednosti v izraz za interval zaupanja za $\sigma$.
    \begin{equation*}
      I_{\sigma}=\left[0.7\sqrt{\frac{19}{32.9}},~ 0.7\sqrt{\frac{19}{8.91}}\right]=[0.53, ~ 1.02].
    \end{equation*}
    Za stopnjo zaupanja $1-\alpha=0.99$ (oziroma stopnjo tveganja $\alpha=0.01$), dobimo vrednosti $c_1$ in $c_2$
    \begin{equation*}
      c_1=\chi^2_{19;0.005}=6.84,\quad c_2=\chi^2_{19;0.995}=38.6.
    \end{equation*}
    Zamenjamo dobljene vrednosti v izraz za interval zaupanja za $\sigma$.
    \begin{equation*}
      I_{\sigma}=\left[0.7\sqrt{\frac{19}{38.6}},~ 0.7\sqrt{\frac{19}{6.84}}\right]=[0.49, ~ 1.17].
    \end{equation*}
    Opažamo, da smo za višjo stopnjo zaupanja $1-\alpha=0.99$ dobili širši interval zaupanja.
  \end{resitev}
\end{naloga}



\begin{naloga}[vaje]{iz_11}[resena]
  Telesna teža slučajnega vzorca 75 učenk 7. razreda osnovne šole ima naslednjo frekvenčno porazdelitev.
  $$\begin{array}{c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c}
      \textup{teža}~\lbrack kg\rbrack & 39 & 40 & 41 & 42 & 43 & 44 & 45 & 46 & 47 & 48 & 49 & 50 & 51 & 52 & 53 & 54 & 59 \\ \hline
      \textup{št. učencev}            & 1  & 3  & 5  & 8  & 8  & 7  & 9  & 8  & 6  & 6  & 4  & 3  & 2  & 2  & 1  & 1  & 1  \\
    \end{array}$$
  Poišči $95\%$ interval zaupanja za pričakovano vrednost $\mu$ in standardni odklon $\sigma$ teže učenk
  7. razreda.
  \begin{rezultat}
    $\overline{x}=45.5$, $s=3.71$, $I_{\mu}=[45.5-0.86,~ 45.5+0.86]=[44.64,~ 46.36]$, $I_{\sigma}=[3.27, ~ 4.57]$.
  \end{rezultat}
  \begin{resitev}
    Označimo z $X$ težo učenk. V vzorcu velikosti $n=75$ imamo $17$ različnih vrednosti teže učenk, in število učenk (frekvenco) $f_i$, $i=1,2,\ldots 17$,
    za vsako od teh vrednosti. Vzorčno povprečje in popravljeno vzorčno varianco za tabelarno dane podatke najdemo na naslednji način:
    \begin{align*}
       & \overline{x}=\frac{1}{75}\sum_{i=1}^{17}f_ix_i=\frac{1\cdot 39+3\cdot 40+\ldots +1\cdot 59}{75}=\frac{3413}{75}=45.5,                                          \\
       & s^2=\frac{1}{74}\sum_{i=1}^{17}f_i (x_i-\overline{x})^2=\frac{1\cdot (39-45.5)^2+3\cdot (40-45.5)^2+\ldots +1\cdot(59-45.5)^2}{74}=\frac{1018.75}{74}=13.7669.
    \end{align*}
    Potem je popravljeni vzorčni standardni odklon enak $s=\sqrt{s^2}=3.71$. Izbrana je stopnja zaupanja $1-\alpha=0.95$ (oziroma stopnja tveganja $\alpha=0.05$). Interval zaupanja za pričakovano vrednost $\mu$ je $I_{\mu}=\left[\overline{x}-c\frac{s}{\sqrt{n}},\; \overline{x}+c\frac{s}{\sqrt{n}}\right]$, kjer je
    \begin{equation*}
      c=t_{n-1; 1-\frac{0.05}{2}}=t_{74; 0.975}\approx t_{60; 0.975}=2
    \end{equation*}
    (vzeli smo vrednost iz tabele kvantilov Studentove porazdelitve, z najbližimi prostostnimi stopnjami).
    Zamenjamo dobljene vrednosti v izraz za interval zaupanja za $\mu$.
    \begin{equation*}
      I_{\mu}=\left[45.5-2\frac{3.71}{\sqrt{75}},~ 45.5+2\frac{3.71}{\sqrt{75}} \right]=[45.5-0.86,~ 45.5+0.86]=[44.64,~ 46.36].
    \end{equation*}
    Interval zaupanja za standardni odklon $\sigma$ je
    $I_{\sigma}=\left[s\sqrt{\frac{n-1}{c_2}},\; s\sqrt{\frac{n-1}{c_1}}\right]$, kjer sta
    \begin{align*}
       & c_1=\chi^2_{n-1; \frac{0.05}{2}}=\chi^2_{74;0.025}=\approx \chi^2_{70;0.025}=48.8, \\
       & c_2=\chi^2_{n-1; 1-\frac{0.05}{2}}=\chi^2_{74;0.975}=\approx \chi^2_{70;0.975}=95
    \end{align*}
    (vzeli smo vrednost iz tabele kvantilov $\chi^2$ porazdelitve, z najbližimi prostostnimi stopnjami).
    Zamenjamo dobljene vrednosti v izraz za interval zaupanja za $\sigma$.
    \begin{equation*}
      I_{\sigma}=\left[3.71\sqrt{\frac{74}{95}},~ 3.71\sqrt{\frac{74}{48.8}} \right]=[3.27, ~ 4.57].
    \end{equation*}
  \end{resitev}
\end{naloga}

\begin{naloga}{iz_12}
  Izbrali smo slučajni vzorec 30 radio sprejemnikov General Electric (GE) in zabeležili njihovo življenjsko dobo (v h). Izračunali smo vzorčno povprečje 1210 ur in popravljeni vzorčni standardni odklon 92 ur. Izračunaj $90\%$ interval zaupanja za pričakovano vrednost $\mu$ in standardni odklon $\sigma$ življenske dobe populacije GE radio sprejemnikov.
  \begin{rezultat}
    $I_{\mu}=[1210-27.63,~1210+27.63]=[1182.37, ~1237.63]$, $I_{\sigma}=[75.94, ~ 117.73]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}

\begin{naloga}{iz_13}
  Antropologinja je izmerila višine naključnega vzorca $60$ moških perujskega plemena. Izračunala je
  vzorčno povprečje 165~cm in popravljeni vzorčni standardni odklon 4.6~cm. Izračunaj $99\%$ interval zaupanja za pričakovano vrednost $\mu$ in standardni odklon $\sigma$ višine vseh moških tega plemena.
  \begin{rezultat}
    $I_{\mu}=[165-1.53,~165+1.53]=[163.47,~ 166.53]$, $I_{\sigma}=[3.71, ~ 5.99]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}








%\begin{naloga}{iz_10}
%Za $X\!\sim\!\mathrm{N}(\mu, \sigma)$ imamo vzorec $124,129,126,122,124$. Določi $90\%$ interval zaupanja za $\sigma$.
%\begin{rezultat}
%\end{rezultat}
%\begin{resitev}
%$[1.72,6.29]$
%\end{resitev}
%\end{naloga}


%\begin{naloga}{hipo8} +++
%	V tovarni polnijo vrece peska s težo 10kg. Kontrolor naključno stehta $10$ vreč in dobi vrednosti $8.76,\, 9.83,\, 9.91,\, 10.97,\, 10.24,\, 8.15,\, 9.88,\, 11.37,\, 10.70,\, 10.69$ (v kg). Določi
%	\begin{enumerate}
%		\item vzorčno povprečje in vzorčni odklon meritev.
%		\item interval zaupanja za povprečje pri $\gamma\!=\!0.98$.
%		\item interval zaupanja za odklon pri $\gamma\!=\!0.95$.
%	\end{enumerate}
%	%\begin{rezultat}
%	%\end{rezultat}
%	\begin{resitev}\end{resitev}
%\end{naloga}



% =======
% TEORIJA
% =======
\begin{center}\begin{fminipage}{14cm}
    \textbf{Interval zaupanja za delež \emph{p}}
    Naj bo $p$ delež populacije z določeno lastnostjo in naj bo $(X_1,X_2,\ldots, X_n)$
    enostavni slučajni vzorec, pri čemer $X_i=1$ z verjetnostjo $p$ in $X_i=0$ z verjetnostjo $1-p$.


    Neznani delež $p$ ocenjujemo z vzorčnim deležem $\hat{p}=\frac{1}{n}\sum_{i=1}^n X_i$. Interval zaupanja za delež $p$ s stopnjo zaupanja $1-\alpha$ je enak
    \begin{equation*}
      I_p=\left[\hat{p}-c\sqrt{\frac{\hat{p}(1-\hat{p})}{n}},\; \hat{p}+c\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}\right],
    \end{equation*}
    kjer je $c=\Fi^{-1}\left(1-\frac{\alpha}{2}\right)$ kvantil standardne normalne porazdelitve.

    Za uporabo zgornjega izraza za interval zaupanja za $p$ potrebujemo dovolj veliki vzorec, za aproksimacijo porazdelitve vzorčnega deleža z normalno porazdelitvijo.
  \end{fminipage}\end{center}


\begin{naloga}[vaje]{iz_14}[resena]
  V slučajnem vzorcu $500$ državljanov RS je bilo $452$ desničarjev in $48$ levičarjev. Izračunaj $95\%$ interval zaupanja za delež levičarjev.
  \begin{rezultat}
    $\hat{p}=0.096, I_p=[0.096-0.026, ~ 0.096+0.026]=[0.070,~ 0.122]$.
  \end{rezultat}
  \begin{resitev}
    Vzorec je velikosti $n=500$ in vzorčni delež je $\hat{p}=\frac{48}{500}=0.096$. Izbrana je
    stopnja zaupanja $1-\alpha=0.95$ (oziroma stopnjo tveganja $\alpha=0.05$). Interval zaupanja za delež levičarjev $p$ je enak
    $I_p=\left[\hat{p}-c\sqrt{\frac{\hat{p}(1-\hat{p})}{n}},\; \hat{p}+c\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}\right]$,
    kjer je
    \begin{equation*}
      c=\Fi^{-1}\left(1-\frac{0.05}{2}\right)=\Fi^{-1}(0.975)=1.96.
    \end{equation*}
    Zamenjamo dobljene vrednosti v izraz za interval zaupanja za $p$.
    \begin{align*}
       & I_p=\left[0.096-1.96\sqrt{\frac{0.096\cdot 0.904}{500}},~0.096+1.96\sqrt{\frac{0.096\cdot 0.904}{500}} \right]= \\
       & =[0.096-0.026, ~ 0.096+0.026]=[0.070,~ 0.122].
    \end{align*}
  \end{resitev}
\end{naloga}

%\begin{naloga}{iz_15}
%Na vzorcu $60$ nogometašev jih $15$ enajstmetrovke strelja z levo nogo.
%Poišči $95\%$ interval zaupanja za delež nogometašev, ki strelja z levo nogo.
%\begin{rezultat}
%\end{rezultat}
%\begin{resitev}
%\end{resitev}
%\end{naloga}

\begin{naloga}[vaje]{iz_14a}
  Delež uporabnikov IPhone med mobilnimi telefoni je v Sloveniji enak $27\%$.
  Pri različnih vzorcih velikosti $1500$ smo dobili naslednje podatke za vzorčne deleže:
  \begin{tabular}{cccccccccc}
    $\hat{p}$ & $28.5$ & $26.7$ & $22.8$ & $27.4$ & $26.6$ & $26.2$ & $27.3$ & $27.9$ & $26.8$
  \end{tabular}

  Na istem grafu prikaži dejanski delež in $95\%$ intervale zaupanja za vzorčne deleže. Koliko intervalov ne vsebuje pravega parametra?
\end{naloga}

%
%\begin{naloga}{iz_14}
%Izmed $100$ (oz. $10000$) poskusov jih je uspelo $20$ (oz. $2000$).
%\begin{enumerate}
%\item Določi $95\%$ interval zaupanja za verjetnost, da poskus uspe.
%\item Kako velik vzorec moramo vzeti, da bo interval zaupanja dolžine $0.05$? 
%\end{enumerate}
%\begin{rezultat}
%\end{rezultat}
%\begin{resitev}
%\begin{enumerate}
%\item $[0.12,0.28]$ oz. $[0.19,0.21]$
%\item $984$
%\end{enumerate}
%\end{resitev}
%\end{naloga}






% =======
% TEORIJA
% =======
%\begin{center}\begin{fminipage}{0.95\textwidth}
%\textbf{Opomba:} V formulah vidimo, da večja kot je velikost vzorca $n$ oz. $m$, manjša je širina intervala $2\Delta$ (torej bolj natančna je ocena parametra), in večja kot je stopnja zaupanja $1\!-\!\alpha$, večja je $2\Delta$.
%\end{fminipage}\end{center}




\begin{naloga}{iz_15}
  V anketi je sodelovalo 1112 ljudi. Od teh jih je 701 menilo, da predsednik dobro opravlja svoje delo. Poišči 99\% interval zaupanja za delež ljudi, ki menijo, da predsednik dela dobro.
  \begin{rezultat}
    $\hat{p}=0.63, I_p=[0.63-0.037, ~ 0.63+0.037]=[0.593,~ 0.667]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}


\begin{naloga}{iz_16}
  Uvoznik vina je dobil priložnost poceni nakupa velike količine vina
  Chateau Lafite Rothschild letnik 1947. Zaradi starosti vina so se nekatere buteljke vina spremenile v kis (po videzu buteljke se ne more zaključiti, katere so pokvarjene).
  Uvoznik se je odločil pri nakupu naključno izbrati
  200 buteljk. Od teh je bilo 18 pokvarjenih. Izračunaj 90\% interval zaupanja za delež pokvarjenih buteljk vina.
  \begin{rezultat}
    $\hat{p}=0.09, I_p=[0.09-0.033, ~ 0.09+0.033]=[0.057, ~0.123]$.
  \end{rezultat}
  \begin{resitev}
  \end{resitev}
\end{naloga}

\begin{naloga}[vaje]{iz_17}[resena]
  Podjetje za raziskovanje trga želi ugotoviti delež gospodinjstev,
  ki gledajo ameriški nogomet. Po načrtu bodo uporabili telefonsko anketo
  naključno izbranih gospodinjstev. Koliko velik vzorec potrebujejo,
  če želijo biti $95\%$ prepričani, da je dobljena ocena deleža natančna na
  $\pm 0.02$?
  \begin{rezultat}
    $\epsilon=c\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}\leq 0.02$, $n=2401$.
  \end{rezultat}
  \begin{resitev}
    Interval zaupanja za delež $p$ je enak $I_p=\left[\hat{p}-c\sqrt{\frac{\hat{p}(1-\hat{p})}{n}},\; \hat{p}+c\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}\right]$,
    kar lahko tudi zapišemo $p\in[\hat{p}-\epsilon, ~ \hat{p}-\epsilon]$, kjer je $\epsilon=c\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}$. Dano je
    $\epsilon\leq 0.02$. Iz tabele normalne porazdelitve dobimo $c=\Fi^{-1}(0.975)=1.96$.
    Vzorčni delež $\hat{p}\in [0,1]$ in velja $\hat{p}(1-\hat{p})\leq \frac{1}{4}$
    (maksimalna vrednost $\hat{p}(1-\hat{p})$ je $\frac{1}{4}$ za $\hat{p}=\frac{1}{2}$).
    Dobimo
    \begin{equation*}
      \epsilon=1.96\sqrt{\frac{\hat{p}(1-\hat{p})}{n}}\leq 1.96\sqrt{\frac{1}{4n}}\leq 0.02.
    \end{equation*}
    Potem je $\sqrt{n}\geq 49$, oziroma $n\geq 2401$. Najmanjša velikost vzorca, ki zadovoljuje to neenakost, je $n=2401$.

  \end{resitev}
\end{naloga}

%\begin{naloga}{hipo5}
%	Z anketo bi radi ocenili delež maturantov, ki bodo nadaljevali študij v tujini.
%	\begin{enumerate}
%		\item Za vzorec $n\!=\!200$ oceni širino intervala zaupanja za vzorcni delež pri $\gamma\!=\!0.95$.
%		\item Kolikšen naj bo vzorec maturantov, da bo ocena deleža na 1\% natančna z verjetnostjo 0.95. Predpostavi, da je delež dijakov, ki bodo študirali v tujini manj kot 10\%.
%	\end{enumerate}
%	%\begin{rezultat}
%	%\end{rezultat}
%	\begin{resitev}\end{resitev}
%\end{naloga}

%\begin{naloga}{hipo9}
%Študent računalništva razvije program za optično prepoznavanje znakov (OCR). Program spusti skozi nekaj lepo skeniranih besedil in izračuna odstotke prepoznanih znakov. Rezultati so sledeči: $91\%,\, 94\%,\, 80\%,\, 88\%,\, 87\%,\, 85\%,\, 90\%,\, 89\%$.
%\begin{enumerate}
%\item Koliko znakov program prepozna v povprečju? Kolikšen je odklon?
%\item Izračunaj interval zaupanja za odstotek prepoznanih znakov s stopnjo zaupanja $95$\%.
%\end{enumerate}
%%\begin{rezultat}
%%\end{rezultat}
%\begin{resitev}\end{resitev}
%\end{naloga}
